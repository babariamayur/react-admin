import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { persistState } from "redux-devtools";
import rootReducer from "../reducers";
import DevTools from "../root/DevTools";
import { apiMiddleware } from "../middleware/redux-api-middleware";

const middleware = [apiMiddleware, thunk];
const enhancer = [
  DevTools.instrument(),
  persistState(window.location.href.match(/[?&]debug_session=([^&#]+)\b/))
];
const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancer);

export default function configureStore(initialState) {
  const store = createStore(rootReducer, initialState, composedEnhancers);
  if (module.hot) {
    module.hot.accept("../reducers", () =>
      store.replaceReducer(require("../reducers").default)
    );
  }
  return store;
}
