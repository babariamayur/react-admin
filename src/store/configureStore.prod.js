import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../reducers";

const middleware = [thunk];
const enhancer = [];
const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancer);
export default function configureStore(initialState) {
  return createStore(rootReducer, initialState, composedEnhancers);
}
