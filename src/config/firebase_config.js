import firebase from "firebase";
// Initialize Firebase
const config = {
  apiKey: "AIzaSyB_LBY65pretYRsgDbKcHxiWGOWjfPLpMQ",
  authDomain: "react-admin-7b185.firebaseapp.com",
  databaseURL: "https://react-admin-7b185.firebaseio.com",
  projectId: "react-admin-7b185",
  storageBucket: "react-admin-7b185.appspot.com",
  messagingSenderId: "349416996931"
};
firebase.initializeApp(config);
export const auth = firebase.auth();
export const storage = firebase.storage();
export const database = firebase.database();
export const timestamp = firebase.database.ServerValue.TIMESTAMP;
