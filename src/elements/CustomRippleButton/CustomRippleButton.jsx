import React, { Component } from 'react'

class Ripple extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animate: false,
            width: 0,
            height: 0,
            top: 0,
            left: 0
          }
      
    }
    render() { 
        return (
            <div>
                {this.props.children}
                <div className={"Ripple " + (this.state.animate ? "is-reppling" : "")} ref="ripple" style={{
                    top: this.state.top+"px",
                    left: this.state.left+"px",
                    width: this.state.width+"px",
                    height: this.state.height+"px"
                }}></div>
            </div>
           
        )
    }
}
 
export default Ripple;