import Dashboard from "../views/Dashboard/Dashboard";
import AuthContainer from "../containers/AuthContainer/AuthContainer";
import RolesContainer from "../containers/RolesContainer/RolesContainer";
import RoleContainer from "../containers/RolesContainer/RoleContainer";
import PermissionsContainer from "../containers/PermissionsContainer/PermissionsContainer";
import PermissionContainer from "../containers/PermissionsContainer/PermissionContainer";
import UsersContainer from "../containers/UsersContainer/UsersContainer";
import UserContainer from "../containers/UsersContainer/UserContainer";

const appRoutes = [
  { hidden: true, path: "/login", name: "Login", component: AuthContainer },
  {
    secure: true,
    path: "/dashboard",
    name: "dashboard",
    icon: "material-icons",
    icon_title: "dashboard",
    component: Dashboard
  },
  {
    collapse: true,
    name: "users_manage",
    icon: "pe-7s-user",
    path: ["/users"],
    views: [
      {
        hidden: true,
        secure: true,
        path: "/users/new/:id?",
        name: "new_user",
        component: UserContainer
      },
      {
        secure: true,
        path: "/users",
        name: "users",
        component: UsersContainer
      },
      {
        hidden: true,
        secure: true,
        exact: true,
        path: "/roles/new/:id?",
        name: "new_role",
        component: RoleContainer
      },
      {
        exact: true,
        secure: true,
        path: "/roles",
        name: "roles",
        component: RolesContainer
      },
      {
        hidden: true,
        secure: true,
        exact: true,
        path: "/permissions/new/:id?",
        name: "new_permission",
        component: PermissionContainer
      },
      {
        secure: true,
        path: "/permissions",
        name: "permission",
        component: PermissionsContainer
      }
    ]
  },
  { redirect: true, path: "/", to: "/dashboard", name: "Dashboard" }
];

export default appRoutes;
