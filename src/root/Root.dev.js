import React, { Component } from "react";
import PropTypes from "prop-types";
import { Provider } from "react-redux";
import { HashRouter } from "react-router-dom";
import { IntlProvider } from "react-redux-multilingual";
import DevTools from "./DevTools";
import AppContainer from "../containers/AppContainer/AppContainer";
import translations from "../translations/Translations";

export default class Root extends Component {
  render() {
    const { store } = this.props;
    return (
      <Provider store={store}>
        <IntlProvider translations={translations}>
          <div>
            <HashRouter>
              <AppContainer />
            </HashRouter>
            <DevTools />
          </div>
        </IntlProvider>
      </Provider>
    );
  }
}
Root.propTypes = {
  store: PropTypes.object
};
