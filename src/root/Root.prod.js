import React, { Component } from "react";
import { Provider } from "react-redux";
import { HashRouter, Switch } from "react-router-dom";
import { IntlProvider } from "react-redux-multilingual";
import PropTypes from "prop-types";
import AppContainer from "../containers/AppContainer/AppContainer";

import translations from "../translations/Translations";

export default class Root extends Component {
  render() {
    const { store } = this.props;
    return (
      <Provider store={store}>
        <IntlProvider translations={translations}>
          <HashRouter>
            <Switch>
              <AppContainer />
            </Switch>
          </HashRouter>
        </IntlProvider>
      </Provider>
    );
  }
}
Root.propTypes = {
  store: PropTypes.object
};
