import CALL_API from "./CALL_API";
import _ from "lodash";

function isCALLAPI(action) {
  return _.isPlainObject(action) && action.hasOwnProperty(CALL_API);
}

export { isCALLAPI };
