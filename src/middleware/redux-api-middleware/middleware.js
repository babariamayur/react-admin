import CALL_API from "./CALL_API";
import { RequestError } from "./errors";
import { isCALLAPI } from "./validation";
import { normalizeTypeDescriptors, actionWith } from "./util";

function apiMiddleware({ getState }) {
  return next => action => {
    if (!isCALLAPI(action)) return next(action);
    return async () => {
      const callAPI = action[CALL_API];
      const {
        endpoint,
        body,
        headers,
        options = {},
        fetch: doFetch = fetch
      } = callAPI;
      const { method, types } = callAPI;
      const [requestType, successType, failureType] = normalizeTypeDescriptors(
        types
      );
      try {
        const response = await doFetch(endpoint, {
          ...options,
          method,
          body: body || undefined,
          headers: headers || {}
        });
        if (response.ok) {
          return next(
            await actionWith(successType, [action, getState(), response])
          );
        }
        return next(
          await actionWith(
            {
              ...failureType,
              error: true
            },
            [action, getState(), response]
          )
        );
      } catch (e) {
        return next(
          await actionWith(
            {
              ...requestType,
              payload: new RequestError(e.message),
              error: true
            },
            [action, getState()]
          )
        );
      }
    };
  };
}
export { apiMiddleware };
