class InvalidRSAA extends Error {
  constructor(validationErrors) {
    super();
    this.name = "InvalidRSAA";
    this.message = "Invalid RSAA";
    this.validationErrors = validationErrors;
  }
}
class InternalError extends Error {
  constructor(message) {
    super();
    this.name = "InternalError";
    this.message = message;
  }
}
class RequestError extends Error {
  constructor(message) {
    super();
    this.name = "RequestError";
    this.message = message;
  }
}
class ApiError extends Error {
  constructor(status, statusText, response) {
    super();
    this.name = "ApiError";
    this.status = status;
    this.statusText = statusText;
    this.response = response;
    this.message = `${status} - ${statusText}`;
  }
}
export { InternalError, RequestError, InvalidRSAA, ApiError };
