import _ from "lodash";
import React from "react";
import PropTypes from "prop-types";
import { Col, Row, Form, DropdownButton, MenuItem } from "react-bootstrap";
import { required, email, confirmation, length } from "redux-form-validators";
import Loader from "halogen";
import BlockUi from "react-block-ui";
import "react-block-ui/style.css";
import { FormInputs } from "../../../components/FormInputs/FormInputs";
import Button from "../../../elements/CustomButton/CustomButton";
import { Card } from "../../../components/Card/Card";

const UsersForm = props => (
  <Form onSubmit={props.handleSubmit(props.handleSubmitForm)}>
    <Card
      cardHeader={
        <div className="card-header card-header-rose card-header-text">
          <div className="card-text" data-background-color="rose">
            <h4 className="card-title">
              {props.id
                ? props.translate("update_user_form", {
                    name: ""
                  })
                : props.translate("create_user_form")}
            </h4>
          </div>
        </div>
      }
      cardFooter={<div className="footer text-center" />}
    >
      <BlockUi
        tag="div"
        blocking={props.isLoading}
        loader={<Loader.ClipLoader color="#d81b60" />}
      >
        <FormInputs
          ncols={["col-md-12"]}
          proprieties={[
            {
              label: props.translate("user_name"),
              type: "text",
              name: "displayName",
              autoComplete: "off",
              validate: [required()]
            }
          ]}
        />
        <FormInputs
          ncols={["col-md-12"]}
          proprieties={[
            {
              label: props.translate("user_email"),
              type: "text",
              name: "email",
              autoComplete: "off",
              validate: [required(), email()]
            }
          ]}
        />
        <FormInputs
          ncols={["col-md-12"]}
          proprieties={[
            {
              label: props.translate("user_password"),
              type: "password",
              name: "password",
              autoComplete: "off",
              validate: props.id ? [] : [required(), length({ min: 6 })]
            }
          ]}
        />
        <FormInputs
          ncols={["col-md-12"]}
          proprieties={[
            {
              label: props.translate("user_c_password"),
              type: "password",
              name: "cPassword",
              autoComplete: "off",
              validate: props.id
                ? []
                : [
                    required(),
                    confirmation({ field: "password", fieldLabel: "Password" })
                  ]
            }
          ]}
        />
        <DropdownButton
          id="dropdown-roles-ids"
          bsStyle="info"
          title={props.selectedRole.roleName}
          onSelect={props.handleDropdownButton}
        >
          {_.map(props.roles, (role, key) => (
            <MenuItem key={key} eventKey={{ roleName: role.role_name, key }}>
              {role.role_name}
            </MenuItem>
          ))}
        </DropdownButton>
        <Row>
          <Col md={12}>
            <Button rose fill type="submit">
              {props.translate("submit")}
            </Button>
          </Col>
        </Row>
      </BlockUi>
    </Card>
  </Form>
);

UsersForm.propTypes = {
  translate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleSubmitForm: PropTypes.func.isRequired,
  handleDropdownButton: PropTypes.func.isRequired,
  id: PropTypes.string,
  isLoading: PropTypes.bool,
  roles: PropTypes.objectOf(PropTypes.any),
  selectedRole: PropTypes.objectOf(PropTypes.any).isRequired
};
UsersForm.defaultProps = {
  id: "",
  roles: [],
  isLoading: false
};
export default UsersForm;
