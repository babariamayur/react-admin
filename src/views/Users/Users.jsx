import _ from "lodash";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Grid, Col, Row } from "react-bootstrap";
import Form from "./Form/Form";

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRole: {
        key: 0,
        roleName: "-- Select Roles --"
      }
    };
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
    this.handleDropdownButton = this.handleDropdownButton.bind(this);
  }
  componentWillMount() {
    const { match, fetchRoles, getUser } = this.props;
    if (match.params.id) {
      this.setState({ key: match.params.id });
      getUser(match.params.id);
    }
    fetchRoles();
  }
  componentWillReceiveProps(nextProps) {
    const { user } = nextProps.users;
    if (!_.isEmpty(user)) {
      if (this.state.selectedRole.key === 0 && user.hasOwnProperty("roleId")) {
        this.setState({
          selectedRole: {
            key: user.roleId,
            roleName: nextProps.users.roles[user.roleId].role_name
          }
        });
      }
    }
  }
  handleSubmitForm(value) {
    const { createUser, updateUser, history } = this.props;
    const roleId = this.state.selectedRole.key;
    if (this.state.key) {
      updateUser(
        {
          ...value,
          emailVerified: false,
          disabled: false,
          roleId,
          key: this.state.key
        },
        () => history.push("/users")
      );
      return true;
    }
    createUser(
      {
        ...value,
        emailVerified: false,
        disabled: false,
        roleId
      },
      () => history.push("/users")
    );
    return true;
  }
  handleDropdownButton(value) {
    this.setState({ selectedRole: value });
  }
  render() {
    const { handleSubmit, translate, users } = this.props;
    const { isLoading, roles } = users;
    return (
      <Grid fluid>
        <Row>
          <Col sm={12}>
            <Form
              handleSubmit={handleSubmit}
              handleSubmitForm={this.handleSubmitForm}
              handleDropdownButton={this.handleDropdownButton}
              selectedRole={this.state.selectedRole}
              id={this.state.key}
              isLoading={isLoading}
              roles={roles}
              translate={translate}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}
Users.propTypes = {
  getUser: PropTypes.func.isRequired,
  createUser: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  fetchRoles: PropTypes.func.isRequired,
  match: PropTypes.objectOf(PropTypes.any),
  history: PropTypes.objectOf(PropTypes.any),
  users: PropTypes.objectOf(PropTypes.any).isRequired
};
Users.defaultProps = {
  match: {},
  history: {}
};
export default Users;
