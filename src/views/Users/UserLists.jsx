import _ from "lodash";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { Grid, Row, Col, Table } from "react-bootstrap";
import SweetAlert from "react-bootstrap-sweetalert";
import BlockUi from "react-block-ui";
import "react-block-ui/style.css";
import Moment from "moment";
import Loader from "halogen";
import { Card } from "../../components/Card/Card";
import Button from "../../elements/CustomButton/CustomButton";

class UserLists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: null
    };
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
  }
  componentWillMount() {
    const { fetchUsers } = this.props;
    fetchUsers();
  }
  handleDeleteConfirm(params) {
    const { deleteUser } = this.props;
    this.setState({
      alert: (
        <SweetAlert
          warning
          showCancel
          confirmBtnText="Yes, delete it!"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="Are you sure?"
          onConfirm={() => {
            this.setState({ alert: null });
            deleteUser(params);
          }}
          onCancel={() => this.setState({ alert: null })}
        >
          You will not be able to recover this user!
        </SweetAlert>
      )
    });
  }
  render() {
    const { translate, users, hasUserPermission } = this.props;
    const { isLoading, data } = users;
    return (
      <Grid fluid>
        <Row>
          {hasUserPermission("Write") && (
            <Col md={12}>
              <Link
                to="/users/new"
                className="btn btn-fill btn-success pull-right"
              >
                {translate("create_button", { name: translate("u_label") })}
              </Link>
            </Col>
          )}
          <Col md={12}>
            <Card
              cardHeader={
                <div className="card-header card-header-icon">
                  <div className="card-icon" data-background-color="rose">
                    <i className="material-icons">assignment</i>
                  </div>
                  <h4 className="card-title">
                    {translate("lists", { name: translate("u_label") })}
                  </h4>
                </div>
              }
            >
              <BlockUi
                tag="div"
                blocking={isLoading}
                loader={<Loader.ClipLoader color="#d81b60" />}
              >
                <Table responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>User Name</th>
                      <th>Email</th>
                      <th>Email Verified</th>
                      <th>Role</th>
                      <th>Created At</th>
                      <th className="text-right">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {hasUserPermission("Write") &&
                      _.map(data, (user, key) => (
                        <tr key={user.uid}>
                          <td>{key + 1}</td>
                          <td>{user.displayName}</td>
                          <td>{user.email}</td>
                          <td>
                            {user.emailVerified ? (
                              <span className="label label-info">Verified</span>
                            ) : (
                              <span className="label label-danger">
                                Pending
                              </span>
                            )}
                          </td>
                          <td>{user.roleName || ""}</td>
                          <td>
                            {Moment(user.createdOn).format("DD/MM/YYYY") || ""}
                          </td>
                          <td className="td-actions text-right">
                            <Link
                              to={`users/new/${user.uid}`}
                              className="btn btn-success"
                            >
                              <i className="material-icons">edit</i>
                            </Link>
                            <Button
                              bsStyle="danger"
                              onClick={() => this.handleDeleteConfirm(user.uid)}
                            >
                              <i className="material-icons">close</i>
                            </Button>
                          </td>
                        </tr>
                      ))}
                  </tbody>
                </Table>
              </BlockUi>
            </Card>
          </Col>
        </Row>
        {this.state.alert}
      </Grid>
    );
  }
}

UserLists.propTypes = {
  translate: PropTypes.func.isRequired,
  fetchUsers: PropTypes.func.isRequired,
  hasUserPermission: PropTypes.func.isRequired,
  users: PropTypes.objectOf(PropTypes.any),
  deleteUser: PropTypes.func.isRequired
};
UserLists.defaultProps = {
  users: []
};
export default UserLists;
