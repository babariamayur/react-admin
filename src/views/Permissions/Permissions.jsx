import React, { Component } from "react";
import PropTypes from "prop-types";
import { Grid, Col, Row } from "react-bootstrap";
import Form from "./Form/Form";

class Permissions extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
  }
  componentWillMount() {
    const { match, fetchPermissions } = this.props;
    if (match.params.id) {
      fetchPermissions(match.params.id);
      this.setState({ key: match.params.id });
    }
  }

  handleSubmitForm(value) {
    const { savePermission, history } = this.props;
    savePermission({ ...value, key: this.state.key }, () =>
      history.push("/permissions")
    );
  }

  render() {
    const { handleSubmit, translate, permissions } = this.props;
    const { isLoading } = permissions;
    return (
      <Grid fluid>
        <Row>
          <Col sm={12}>
            <Form
              handleSubmit={handleSubmit}
              handleSubmitForm={this.handleSubmitForm}
              id={this.state.key}
              isLoading={isLoading}
              translate={translate}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}
Permissions.propTypes = {
  savePermission: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  fetchPermissions: PropTypes.func.isRequired,
  match: PropTypes.objectOf(PropTypes.any),
  history: PropTypes.objectOf(PropTypes.any),
  permissions: PropTypes.objectOf(PropTypes.any).isRequired
};
Permissions.defaultProps = {
  match: {},
  history: {}
};
export default Permissions;
