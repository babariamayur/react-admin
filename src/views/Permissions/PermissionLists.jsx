import _ from "lodash";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Grid, Row, Col, Table } from "react-bootstrap";
import SweetAlert from "react-bootstrap-sweetalert";
import BlockUi from "react-block-ui";
import "react-block-ui/style.css";
import Moment from "moment";
import Loader from "halogen";
import { Card } from "../../components/Card/Card";

class PermissionLists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: null
    };
    this.onDeletePermission = this.onDeletePermission.bind(this);
  }
  componentWillMount() {
    const { fetchPermissions } = this.props;
    fetchPermissions();
  }

  onDeletePermission(params) {
    const { deletePermission } = this.props;
    this.setState({
      alert: (
        <SweetAlert
          warning
          showCancel
          confirmBtnText="Yes, delete it!"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="Are you sure?"
          onConfirm={() => {
            this.setState({ alert: null });
            deletePermission(params);
          }}
          onCancel={() => this.setState({ alert: null })}
        >
          You will not be able to recover this permission!
        </SweetAlert>
      )
    });
  }

  render() {
    const { translate, permissions } = this.props;
    const { isLoading } = permissions;
    const data = permissions
      ? _.map(permissions.data, (permission, key) => ({
          key,
          ...permission
        }))
      : [];

    return (
      <Grid fluid>
        <Row>
          {/* <Col md={12}>
            <Link
              to="/permissions/new"
              className="btn btn-fill btn-success pull-right"
            >
              {" "}
              {translate("create_button", { name: translate("p_label") })}
            </Link>
          </Col> */}
          <Col md={12}>
            <Card
              cardHeader={
                <div className="card-header card-header-icon">
                  <div className="card-icon" data-background-color="rose">
                    <i className="material-icons">assignment</i>
                  </div>
                  <h4 className="card-title">
                    {translate("lists", { name: translate("p_label") })}
                  </h4>
                </div>
              }
            >
              <BlockUi
                tag="div"
                blocking={isLoading}
                loader={<Loader.ClipLoader color="#d81b60" />}
              >
                <Table responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th className="text-right">Permission Type</th>
                      <th className="text-right">Created At</th>
                    </tr>
                  </thead>
                  <tbody>
                    {data.map((val, key) => (
                      <tr key={val.key}>
                        <td>{key + 1}</td>
                        <td className="text-right">{val.permission_type}</td>
                        <td className="text-right">
                          {Moment(val.created_at).format("DD/MM/YYYY")}
                        </td>
                        {/* <td className="td-actions text-right">
                           <Link
                            to={`permissions/new/${val.key}`}
                            className="btn btn-success"
                          >
                            <i className="material-icons">edit</i>
                          </Link>
                          <Button
                            bsStyle="danger"
                            onClick={() => this.onDeletePermission(val)}
                          >
                            <i className="material-icons">close</i>
                         </Button> 
                        </td> */}
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </BlockUi>
            </Card>
          </Col>
        </Row>
        {this.state.alert}
      </Grid>
    );
  }
}

PermissionLists.propTypes = {
  translate: PropTypes.func.isRequired,
  fetchPermissions: PropTypes.func.isRequired,
  deletePermission: PropTypes.func.isRequired,
  permissions: PropTypes.objectOf(PropTypes.any)
};
PermissionLists.defaultProps = {
  permissions: {}
};
export default PermissionLists;
