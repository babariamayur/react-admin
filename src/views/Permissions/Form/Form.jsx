import React from "react";
import PropTypes from "prop-types";
import { Col, Row, Form } from "react-bootstrap";
import { required } from "redux-form-validators";
import Loader from "halogen";
import BlockUi from "react-block-ui";
import "react-block-ui/style.css";
import { FormInputs } from "../../../components/FormInputs/FormInputs";
import Button from "../../../elements/CustomButton/CustomButton";
import { Card } from "../../../components/Card/Card";

const PermissionsForm = props => (
  <Form onSubmit={props.handleSubmit(props.handleSubmitForm)}>
    <Card
      cardHeader={
        <div className="card-header card-header-rose card-header-text">
          <div className="card-text" data-background-color="rose">
            <h4 className="card-title">
              {props.id
                ? props.translate("update_permission_form")
                : props.translate("create_permission_form")}
            </h4>
          </div>
        </div>
      }
      cardFooter={<div className="footer text-center" />}
    >
      <BlockUi
        tag="div"
        blocking={props.isLoading}
        loader={<Loader.ClipLoader color="#d81b60" />}
      >
        <FormInputs
          ncols={["col-md-12"]}
          proprieties={[
            {
              label: props.translate("permission_type"),
              type: "text",
              name: "permission_type",
              autoComplete: "off",
              validate: [required()]
            }
          ]}
        />
        <Row>
          <Col md={12}>
            <Button rose fill type="submit">
              {props.translate("submit")}
            </Button>
          </Col>
        </Row>
      </BlockUi>
    </Card>
  </Form>
);

PermissionsForm.propTypes = {
  translate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleSubmitForm: PropTypes.func.isRequired,
  id: PropTypes.string,
  isLoading: PropTypes.bool
};
PermissionsForm.defaultProps = {
  id: "",
  isLoading: false
};

export default PermissionsForm;
