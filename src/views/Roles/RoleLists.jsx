import _ from "lodash";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { Grid, Row, Col, Table } from "react-bootstrap";
import SweetAlert from "react-bootstrap-sweetalert";
import BlockUi from "react-block-ui";
import "react-block-ui/style.css";
import Moment from "moment";
import Loader from "halogen";
import { Card } from "../../components/Card/Card";
import Button from "../../elements/CustomButton/CustomButton";

class RoleLists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: null
    };
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
  }
  componentWillMount() {
    const { fetchRoles } = this.props;
    fetchRoles();
  }
  handleDeleteConfirm(params) {
    const { deleteRole } = this.props;
    this.setState({
      alert: (
        <SweetAlert
          warning
          showCancel
          confirmBtnText="Yes, delete it!"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="Are you sure?"
          onConfirm={() => {
            this.setState({ alert: null });
            deleteRole(params);
          }}
          onCancel={() => this.setState({ alert: null })}
        >
          You will not be able to recover this role!
        </SweetAlert>
      )
    });
  }
  render() {
    const { translate, roles, hasUserPermission } = this.props;
    const { isLoading } = roles;
    const data = roles
      ? _.map(roles.data, (role, key) => ({
          key,
          ...role
        }))
      : [];
    return (
      <Grid fluid>
        <Row>
          <Col md={12}>
            {hasUserPermission("Write") && (
              <Link
                to="/roles/new"
                className="btn btn-fill btn-success pull-right"
              >
                {translate("create_button", { name: translate("r_label") })}
              </Link>
            )}
          </Col>
          <Col md={12}>
            <Card
              cardHeader={
                <div className="card-header card-header-icon">
                  <div className="card-icon" data-background-color="rose">
                    <i className="material-icons">assignment</i>
                  </div>
                  <h4 className="card-title">
                    {translate("lists", { name: translate("r_label") })}
                  </h4>
                </div>
              }
            >
              <BlockUi
                tag="div"
                blocking={isLoading}
                loader={<Loader.ClipLoader color="#d81b60" />}
              >
                <Table responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th className="text-right">Role Name</th>
                      <th className="text-right">Created At</th>
                      <th className="text-right">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {hasUserPermission("Read") ? (
                      data.map((val, key) => (
                        <tr key={val.key}>
                          <td>{key + 1}</td>
                          <td className="text-right">{val.role_name}</td>
                          <td className="text-right">
                            {Moment(val.created_at).format("DD/MM/YYYY") || ""}
                          </td>
                          <td className="td-actions text-right">
                            {hasUserPermission("Write") && (
                              <Link
                                to={`roles/new/${val.key}`}
                                className="btn btn-success"
                              >
                                <i className="material-icons">edit</i>
                              </Link>
                            )}
                            {hasUserPermission("Delete") && (
                              <Button
                                bsStyle="danger"
                                onClick={() => this.handleDeleteConfirm(val)}
                              >
                                <i className="material-icons">close</i>
                              </Button>
                            )}
                          </td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td colSpan="10" className="text-center">
                          You have no permission to read this data.
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              </BlockUi>
            </Card>
          </Col>
        </Row>
        {this.state.alert}
      </Grid>
    );
  }
}

RoleLists.propTypes = {
  translate: PropTypes.func.isRequired,
  fetchRoles: PropTypes.func.isRequired,
  roles: PropTypes.objectOf(PropTypes.any),
  deleteRole: PropTypes.func.isRequired,
  hasUserPermission: PropTypes.func.isRequired
};
RoleLists.defaultProps = {
  roles: {}
};
export default RoleLists;
