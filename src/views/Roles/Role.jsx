import _ from "lodash";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Grid, Col, Row } from "react-bootstrap";
import Form from "./Form/Form";
import appRoutes from "../../routes/app";

class Role extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
  }
  componentWillMount() {
    const {
      match,
      fetchRoles,
      fetchPermissions,
      fetchRoleHasPermission,
      fetchRoleHasRoutes
    } = this.props;
    if (match.params.id) {
      fetchRoles(match.params.id);
      fetchRoleHasPermission(match.params.id);
      fetchRoleHasRoutes(match.params.id);
      this.setState({ key: match.params.id });
    }
    fetchPermissions();
  }
  handleSubmitForm(value) {
    const { saveRole, history } = this.props;
    saveRole({ ...value, key: this.state.key }, () => history.push("/roles"));
  }
  render() {
    const { handleSubmit, translate, roles } = this.props;
    const { isLoading, permissions } = roles;
    const options = _.map(permissions, (permission, key) => ({
      label: permission.permission_type,
      key
    }));
    const routes = [];
    _.map(appRoutes, (route, key) => {
      if (route.secure || route.collapse) {
        if (_.isObject(route.views)) {
          _.map(route.views, (view, keys) => {
            if (!view.hidden) routes.push({ label: view.name, key: keys });
          });
        }
        routes.push({ label: route.name, key });
      }
    });
    return (
      <Grid fluid>
        <Row>
          <Col sm={12}>
            <Form
              handleSubmit={handleSubmit}
              handleSubmitForm={this.handleSubmitForm}
              id={this.state.key}
              permissions={options}
              routes={routes}
              isLoading={isLoading}
              translate={translate}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}
Role.propTypes = {
  saveRole: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  fetchRoles: PropTypes.func.isRequired,
  fetchPermissions: PropTypes.func.isRequired,
  fetchRoleHasPermission: PropTypes.func.isRequired,
  fetchRoleHasRoutes: PropTypes.func.isRequired,
  match: PropTypes.objectOf(PropTypes.any),
  history: PropTypes.objectOf(PropTypes.any),
  roles: PropTypes.objectOf(PropTypes.any).isRequired
};
Role.defaultProps = {
  match: {},
  history: {}
};
export default Role;
