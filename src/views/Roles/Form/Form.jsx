import _ from "lodash";
import React from "react";
import PropTypes from "prop-types";
import { Field } from "redux-form";
import { Col, Row, Form, ControlLabel } from "react-bootstrap";
import { required } from "redux-form-validators";
import Loader from "halogen";
import BlockUi from "react-block-ui";
import "react-block-ui/style.css";
import { FormInputs } from "../../../components/FormInputs/FormInputs";
import Button from "../../../elements/CustomButton/CustomButton";
import { Card } from "../../../components/Card/Card";

const field = ({ input, meta, options, labels }) => {
  const { name, onChange } = input;
  const { touched, error } = meta;
  const inputValue = input.value;
  const checkboxes = _.map(options, ({ key, label }, index) => {
    const handleChange = event => {
      const arr = [...inputValue];
      if (event.target.checked) {
        arr.push(Number.isInteger(key) ? label.toLowerCase() : key);
      } else {
        arr.splice(
          arr.indexOf(Number.isInteger(key) ? label.toLowerCase() : key),
          1
        );
      }
      return onChange(arr);
    };
    const checked = inputValue.includes(
      Number.isInteger(key) ? label.toLowerCase() : key
    );
    return (
      <label
        className="form-check-label"
        htmlFor={`${name}-${index}`}
        key={`checkbox-${index}`}
      >
        <input
          id={`${name}-${index}`}
          className="form-check-input"
          type="checkbox"
          name={`${name}[${index}]`}
          value={Number.isInteger(key) ? label.toLowerCase() : key}
          checked={checked}
          onChange={handleChange}
        />
        {label}
        <span className="checkbox-material">
          <span className="check" />
        </span>
      </label>
    );
  });

  return (
    <Row>
      <Col sm={12}>
        <ControlLabel>{labels}</ControlLabel>
        <div className="checkbox">{checkboxes}</div>
        {touched && error && <p className="error">{error}</p>}
      </Col>
    </Row>
  );
};
const RolesForm = props => (
  <Form onSubmit={props.handleSubmit(props.handleSubmitForm)}>
    <Card
      cardHeader={
        <div className="card-header card-header-rose card-header-text">
          <div className="card-text" data-background-color="rose">
            <h4 className="card-title">
              {props.id
                ? props.translate("update_role_form")
                : props.translate("create_role_form")}
            </h4>
          </div>
        </div>
      }
      cardFooter={<div className="footer text-center" />}
    >
      <BlockUi
        tag="div"
        blocking={props.isLoading}
        loader={<Loader.ClipLoader color="#d81b60" />}
      >
        <FormInputs
          ncols={["col-md-12"]}
          proprieties={[
            {
              label: props.translate("role_name"),
              type: "text",
              name: "role_name",
              autoComplete: "off",
              validate: [required()]
            }
          ]}
        />
        <Field
          type="checkbox"
          name="routes"
          labels={props.translate("module_list")}
          options={props.routes}
          component={field}
        />
        <Field
          type="checkbox"
          name="permission"
          labels={props.translate("permission_list")}
          options={props.permissions}
          component={field}
        />
        <Row>
          <Col md={12}>
            <Button rose fill type="submit">
              {props.translate("submit")}
            </Button>
          </Col>
        </Row>
      </BlockUi>
    </Card>
  </Form>
);

RolesForm.propTypes = {
  translate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleSubmitForm: PropTypes.func.isRequired,
  id: PropTypes.string,
  isLoading: PropTypes.bool,
  permissions: PropTypes.arrayOf(PropTypes.any),
  routes: PropTypes.arrayOf(PropTypes.any),
  hasPermission: PropTypes.arrayOf(PropTypes.any)
};
RolesForm.defaultProps = {
  id: "",
  permissions: [],
  routes: [],
  hasPermission: [],
  isLoading: false
};
export default RolesForm;
