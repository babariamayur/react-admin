import React from "react";
import classnames from "classnames";
import { Grid, Row, Col, Form } from "react-bootstrap";
import { Card } from "../../components/Card/Card";
import { FormInputs } from "../../components/FormInputs/FormInputs";
import Button from "../../elements/CustomButton/CustomButton";

const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value);
const Login = props => (
  <Grid>
    <Row>
      <Col md={4} sm={6} mdOffset={4} smOffset={3}>
        <Form onSubmit={props.handleSubmit(props.handleSubmitForm)}>
          <Card
            className={classnames({
              "card-login": !props.loading,
              "card-login card-hidden": props.loading
            })}
            isLoading={props.isLoading}
            cardCategory
            hCenter="true"
            fCenter="true"
            category={props.translate("or_be_classical")}
            cardHeader={
              <div
                className="card-header text-center"
                data-background-color="blue"
              >
                <h4 className="card-title">{props.translate("login")}</h4>
              </div>
            }
            cardFooter={
              <div className="footer text-center">
                <Button bsStyle="info" simple fill type="submit">
                  {props.translate("lets_go")}
                </Button>
              </div>
            }
          >
            <FormInputs
              ncols={["col-md-12"]}
              proprieties={[
                {
                  label: props.translate("email"),
                  type: "email",
                  name: "email",
                  iconClass: "email",
                  autoComplete: "off",
                  inputGroup: true,
                  validate: email
                }
              ]}
            />
            <FormInputs
              ncols={["col-md-12"]}
              proprieties={[
                {
                  label: props.translate("password"),
                  type: "password",
                  name: "password",
                  inputGroup: true,
                  iconClass: "lock_outline"
                }
              ]}
            />
          </Card>
        </Form>
      </Col>
    </Row>
  </Grid>
);
export default Login;
