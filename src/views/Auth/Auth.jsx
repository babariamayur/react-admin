import React, { Component } from "react";
import PropTypes from "prop-types";
import Notifications from "react-notification-system-redux";

import Login from "./Login";
import LoginBackground from "../../assets/img/login.jpg";
import { style } from "../../variables/Variables";

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
  }
  componentWillMount() {
    const { checkAuthStatus } = this.props;
    checkAuthStatus()
      .then(user => {
        if (user) {
          return this.handleLoggedInState();
        }
        return false;
      })
      .catch(() => this.handleLoggedInState());
  }
  componentDidMount() {
    setTimeout(
      () =>
        this.setState({
          loading: false
        }),
      600
    );
  }
  /**
   * Handle not logged in state
   *
   * @memberof Authentication
   */
  handleLoggedInState() {
    const { history } = this.props;
    history.push("/dashboard");
  }
  handleSubmitForm({ email, password }) {
    const { login, history } = this.props;
    login(email, password, () => history.push("/dashboard"));
  }
  render() {
    const { loading } = this.state;
    const { auth, notifications } = this.props;
    return (
      <div className="off-canvas-sidebar">
        <Notifications notifications={notifications} style={style} />
        <div className="wrapper wrapper-full-page">
          <div className="full-page login-page" data-filter-color="black">
            <div className="content">
              <Login
                {...this.props}
                handleSubmitForm={this.handleSubmitForm}
                loading={loading}
                isLoading={auth.isLoading}
              />
            </div>
            <div
              className="full-page-background"
              style={{ backgroundImage: `url(${LoginBackground})` }}
            />
          </div>
        </div>
      </div>
    );
  }
}
Auth.propTypes = {
  auth: PropTypes.objectOf(PropTypes.any).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  notifications: PropTypes.arrayOf(PropTypes.any).isRequired,
  login: PropTypes.func.isRequired,
  checkAuthStatus: PropTypes.func.isRequired
};
export default Auth;
