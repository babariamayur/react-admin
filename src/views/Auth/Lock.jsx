import React, { Component } from "react";
import { Card } from "../../components/Card/Card";
import { FormInputs } from "../../components/FormInputs/FormInputs";
import Button from "../../elements/CustomButton/CustomButton";
import LoginBackground from "../../assets/img/login.jpg";

class Lock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }
  componentDidMount() {
    setTimeout(() => this.setState({ loading: false }), 600);
  }
  render() {
    const { loading } = this.state;
    const className = !loading ? " card-profile " : "card-profile card-hidden";
    return (
      <div className="off-canvas-sidebar">
        <div className="wrapper wrapper-full-page">
          <div className="full-page lock-page" data-filter-color="black">
            <div className="content">
              <form>
                <Card
                  className={className}
                  hCenter="true"
                  fCenter="true"
                  cardHeader={
                    <div className="card-avatar">
                      <a href="#pablo">
                        <img
                          className="avatar"
                          src="http://demos.creative-tim.com/material-dashboard-pro/assets/img/faces/avatar.jpg"
                          alt="..."
                        />
                      </a>
                    </div>
                  }
                  cardFooter={
                    <div className="card-footer">
                      <Button bsStyle="success" type="submit">
                        Unlock
                      </Button>
                    </div>
                  }
                >
                  <h4 className="card-title">Tania Andrew</h4>
                  <FormInputs
                    InputGroup={false}
                    ncols={["col-md-12"]}
                    proprieties={[
                      {
                        label: "Password",
                        type: "password",
                        bsClass: "form-control",
                        name: "password",
                        autoComplete: "off",
                        iconClass: "lock_outline"
                      }
                    ]}
                  />
                </Card>
              </form>
            </div>
            <div
              className="full-page-background"
              style={{ backgroundImage: `url(${LoginBackground})` }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Lock;
