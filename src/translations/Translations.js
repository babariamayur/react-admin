module.exports = {
  en: {
    locale: "en-US",
    messages: {
      sitename: "FrondEnd Ninja",

      /** * App Links *** */

      dashboard: "Dashboard",
      users_manage: "Users Manage",
      users: "Users",
      profile: "Profile",
      roles: "Roles",
      add_role: "Create a role",
      permission: "Permission",

      /** * App Links *** */

      /** *** Roles table feilds *** */

      r_name: "Role Name",
      r_created: "Created At",
      r_label: "Role",

      /** *** Permission table feilds *** */

      permission_type: "Permission Type",
      p_created: "Created At",
      p_label: "Permission",

      /** *** Users table feilds *** */
      u_label: "User",
      create_user_form: "Create User",
      update_user_form: "Update User {name}",
      user_name: "User Name",
      user_password: "Password",
      user_c_password: "Confirm Password",
      user_email: "Email",

      /** *** Roles table feilds *** */

      /** * Form Label Key ** */

      email: "Email Address",
      password: "Password",
      role_name: "Role Name",

      /** * Form Label Key ** */

      /** Form title */

      create_role_form: "Create Role",
      update_role_form: "Update Role",
      create_permission_form: "Create Permission",
      update_permission_form: "Update Permission",

      /** * Buttons ** */

      lets_go: "Let's go",
      or_be_classical: "Or Be Classical",
      login: "Login",
      submit: "Submit",

      /** **Common Table fields** */

      actions: "Actions",
      create_button: "Create a {name}",
      lists: "{name} lists",
      permission_list: "Permission Lists",
      module_list: "Module Lists"
    }
  },
  ar: {
    locale: "ar",
    messages: {
      sitename: "الجبهة النينجا",
      /** * App Links *** */
      dashboard: "لوحة القيادة",
      users_manage: "إدارة المستخدمين",
      users: "المستخدمين",
      profile: "الملف الشخصي",
      roles: "الأدوار",
      add_role: "قم بإنشاء دور",
      permission: "الإذن",

      /** * App Links *** */

      /** *** Roles table feilds *** */

      r_name: "اسم الدور",
      r_created: "أنشئت في",
      r_label: "وظيفة",

      /** *** Permission table feilds *** */

      permission_type: "نوع الإذن",
      p_created: "أنشئت في",
      p_label: "الإذن",

      /** *** Users table feilds *** */
      u_label: "المستعمل",
      create_user_form: "انشاء المستخدم",
      update_user_form: "تحديث المستخدم {name}",
      user_name: "اسم المستخدم",
      user_password: "كلمه السر",
      user_c_password: "تأكيد كلمة المرور",
      user_email: "البريد الإلكتروني",

      /** *** Roles table feilds *** */

      /** * Form Label Key ** */

      email: "عنوان البريد الإلكتروني",
      password: "كلمه السر",
      role_name: "اسم الدور",

      /** * Form Label Key ** */

      /** Form title */

      create_role_form: "خلق دور",
      update_role_form: "تحديث دور",
      create_permission_form: "خلق إذن",
      update_permission_form: "تحديث إذن",

      /** * Buttons ** */

      lets_go: "لنذهب",
      or_be_classical: "أو كن كلاسيكي",
      login: "تسجيل الدخول",
      submit: "خضع",

      /** **Common Table fields** */

      actions: "أفعال",
      create_button: "أنشئ {name}",
      lists: "قوائم {name}",
      permission_list: "Permission Lists",
      module_list: "Module Lists"
    }
  }
};
