import _ from "lodash";
import { success, error } from "react-notification-system-redux";
import { timestamp, database } from "../../config/firebase_config";
import { actionTypes as a } from "./Actions_types";
import { notificationOpts } from "../../components/Notification/Notification";

/**
 * Permission save actions
 */

const fetchPermissionsRequestedAction = () => ({
  type: a.fetchPermissionsRequested
});

const fetchPermissionsRejectedAction = e => ({
  type: a.fetchPermissionsRejected,
  payload: e
});

const fetchPermissionsFulfilledAction = values => ({
  type: a.fetchPermissionsFulfilled,
  payload: values
});

export const fetchPermissions = params => dispatch => {
  dispatch(fetchPermissionsRequestedAction());
  let promise;
  if (params) {
    promise = database.ref("permissions").child(params);
  } else {
    promise = database.ref("permissions");
  }
  promise
    .once("value")
    .then(snapshot => {
      dispatch(fetchPermissionsFulfilledAction(snapshot.val()));
    })
    .catch(e => {
      dispatch(fetchPermissionsRejectedAction(e));
    });
  return promise;
};
/**
 * Permission save actions
 */
const permissionRequestedAction = () => ({ type: a.savePermissionRequested });
const permissionRejectedAction = e => ({
  type: a.savePermissionRejected,
  payload: e
});
const permissionFulfilledAction = value => ({
  type: a.savePermissionFulfilled,
  payload: value
});

export const exitsFieldValue = (
  promise,
  fieldName,
  fieldValue,
  key
) => dispatch =>
  new Promise((resolve, reject) => {
    promise
      .orderByChild(fieldName)
      .equalTo(fieldValue)
      .once("value")
      .then(snapshot => {
        const value = snapshot.val();
        if (value) {
          if (_.hasIn(value, key)) {
            resolve(value);
            return true;
          }
          dispatch(
            error({
              ...notificationOpts(
                {
                  message: `<b>${fieldValue}</b> already exits!`
                },
                true
              )
            })
          );
          return true;
        }
        return resolve(value);
      })
      .catch(e => {
        reject(e);
      });
  });

export const savePermission = (params, callback) => {
  const key = params.key || "";
  delete params.key;
  return dispatch => {
    dispatch(permissionRequestedAction());
    const promise = database.ref("permissions");
    Promise.resolve()
      .then(() =>
        dispatch(
          exitsFieldValue(
            promise,
            "permission_type",
            params.permission_type,
            key
          )
        )
      )
      .then(() => {
        let roleRefs;
        if (key) {
          roleRefs = promise
            .child(key)
            .update({ ...params, updated_at: timestamp });
        } else {
          roleRefs = promise.push({ ...params, created_at: timestamp });
        }
        roleRefs
          .then(() => {
            promise.once("value").then(snapshot => {
              dispatch(permissionFulfilledAction(snapshot.val()));
            });
            callback();
            dispatch(
              success({
                ...notificationOpts({
                  message: `Permission has been ${
                    key ? "updated" : "created"
                  } successfully.`
                })
              })
            );
          })
          .catch(e => {
            dispatch(permissionRejectedAction(e));
            dispatch(
              error({
                ...notificationOpts(
                  {
                    message: e.message
                  },
                  true
                )
              })
            );
          });
      })
      .catch(e => {
        dispatch(permissionRejectedAction(e));
        dispatch(
          error({
            ...notificationOpts(
              {
                message: e.message
              },
              true
            )
          })
        );
      });
    return promise;
  };
};

// Permission delete action
function deletePermissionRequestedAction() {
  return { type: a.deletePermissionRequested };
}

function deletePermissionRejectedAction() {
  return { type: a.deletePermissionRejected };
}

function deletePermissionFulfilledAction(payload) {
  return { type: a.deletePermissionFulfilled, payload };
}

export const deletePermission = params => {
  const { key } = params;
  return dispatch => {
    dispatch(deletePermissionRequestedAction());

    database
      .ref(`/permissions/${key}`)
      .remove()
      .then(() => {
        database
          .ref("role_has_permissions")
          .once("value")
          .then(snapshot => {
            const data = snapshot.val();
            _.map(data, (value, keys) => {
              const filterValue = _.indexOf(value, key);
              if (filterValue !== -1) {
                database
                  .ref("role_has_permissions")
                  .child(keys)
                  .child(filterValue)
                  .remove();
              }
            });
          });
        database
          .ref("permissions")
          .once("value")
          .then(snapshot => {
            dispatch(deletePermissionFulfilledAction(snapshot.val()));
            dispatch(
              success({
                ...notificationOpts({
                  message: "Permission has been deleted successfully."
                })
              })
            );
          });
      })
      .catch(err => {
        dispatch(deletePermissionRejectedAction(key));
        dispatch(
          error({
            ...notificationOpts(
              {
                message: `Problem while removing role item ${
                  params.permission_type
                }. ${err.message}`
              },
              true
            )
          })
        );
      });
  };
};

export default {
  savePermission
};
