export const actionTypes = {
  /* Fetch Permission */
  fetchPermissionsRequested: "FETCH_PERMISSIONS_REQUESTED",
  fetchPermissionsRejected: "FETCH_PERMISSIONS_REJECTED",
  fetchPermissionsFulfilled: "FETCH_PERMISSIONS_FULFILLED",
  clearProductsRequestedAction: "CLEAR_PERMISSIONS",

  /* Save  Permission */
  savePermissionRequested: "SAVE_PERMISSION_REQUESTED",
  savePermissionRejected: "SAVE_PERMISSION_REJECTED",
  savePermissionFulfilled: "SAVE_PERMISSION_FULFILLED",

  // Delete permission action
  deletePermissionRequested: "DELETE_PERMISSION_REQUESTED",
  deletePermissionRejected: "DELETE_PERMISSION_REJECTED",
  deletePermissionFulfilled: "DELETE_PERMISSION_FULFILLED"
};

export default actionTypes;
