export const actionTypes = {
  /* Fetch Users */
  fetchUsersRequested: "FETCH_USERS_REQUESTED",
  fetchUsersRejected: "FETCH_USERS_REJECTED",
  fetchUsersFulfilled: "FETCH_USERS_FULFILLED",

  /* Fetch User */
  fetchUserRequested: "FETCH_USER_REQUESTED",
  fetchUserRejected: "FETCH_USER_REJECTED",
  fetchUserFulfilled: "FETCH_USER_FULFILLED",

  /* Fetch Roles */
  fetchRolesRequested: "FETCH_ROLES_REQUESTED",
  fetchRolesRejected: "FETCH_ROLES_REJECTED",
  fetchRolesFulfilled: "FETCH_ROLES_FULFILLED",

  /* Create User */
  createUserRequested: "CREATE_USER_REQUESTED",
  createUserRejected: "CREATE_USER_REJECTED",
  createUserFulfilled: "CREATE_USER_FULFILLED",

  /* Update User */
  updateUserRequested: "UPDATE_USER_REQUESTED",
  updateUserRejected: "UPDATE_USER_REJECTED",
  updateUserFulfilled: "UPDATE_USER_FULFILLED",

  /* Delete User */
  deleteUserRequested: "DELETE_USER_REQUESTED",
  deleteUserRejected: "DELETE_USER_REJECTED",
  deleteUserFulfilled: "DELETE_USER_FULFILLED"
};

export default actionTypes;
