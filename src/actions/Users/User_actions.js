import _ from "lodash";
import { success, error } from "react-notification-system-redux";
import { database } from "../../config/firebase_config";
import { actionTypes as a } from "./Actions_types";
import { notificationOpts } from "../../components/Notification/Notification";
/**
 * Users actions
 */
const fetchUsersRequestedAction = () => ({
  type: a.fetchUsersRequested
});

const fetchUsersRejectedAction = e => ({
  type: a.fetchUsersRejected,
  payload: e
});

const fetchUsersFulfilledAction = values => ({
  type: a.fetchUsersFulfilled,
  payload: values
});

export const fetchUsers = () => dispatch => {
  dispatch(fetchUsersRequestedAction());
  return fetch(
    "https://us-central1-react-admin-7b185.cloudfunctions.net/getAllUser"
  )
    .then(response =>
      response.json().then(json => (response.ok ? json : Promise.reject(json)))
    )
    .then(response => {
      const data = response.users;
      database
        .ref("roles")
        .once("value")
        .then(snapshot => {
          const roles = snapshot.val();
          if (roles) {
            const users = _.map(data, value => ({
              ...value,
              roleName: roles[value.roleUid] && roles[value.roleUid].role_name
            }));
            dispatch(fetchUsersFulfilledAction(users));
          } else {
            dispatch(fetchUsersFulfilledAction(response.users));
          }
        })
        .catch(e => {
          dispatch(fetchUsersRejectedAction(e));
          dispatch(
            error({
              ...notificationOpts(
                {
                  message: e.message
                },
                true
              )
            })
          );
        });
    })
    .catch(e => {
      dispatch(fetchUsersRejectedAction(e));
      dispatch(
        error({
          ...notificationOpts(
            {
              message: e.message
            },
            true
          )
        })
      );
    });
};

/**
 * Roles actions
 */

const fetchRolesRequestedAction = () => ({
  type: a.fetchRolesRequested
});

const fetchRolesRejectedAction = e => ({
  type: a.fetchRolesRejected,
  payload: e
});

const fetchRolesFulfilledAction = values => ({
  type: a.fetchRolesFulfilled,
  payload: values
});

export const fetchRoles = params => dispatch => {
  dispatch(fetchRolesRequestedAction());
  let promise;
  if (params) {
    promise = database.ref("roles").child(params);
  } else {
    promise = database.ref("roles");
  }
  promise
    .once("value")
    .then(snapshot => {
      dispatch(fetchRolesFulfilledAction(snapshot.val()));
    })
    .catch(e => {
      dispatch(fetchRolesRejectedAction(e));
    });
  return promise;
};
/**
 * Users actions
 */
const fetchUserRequestedAction = () => ({
  type: a.fetchUserRequested
});

const fetchUserRejectedAction = e => ({
  type: a.fetchUserRejected,
  payload: e
});

const fetchUserFulfilledAction = values => ({
  type: a.fetchUserFulfilled,
  payload: values
});
export const getUser = params => dispatch => {
  dispatch(fetchUserRequestedAction());
  return fetch(
    `https://us-central1-react-admin-7b185.cloudfunctions.net/getUser/${params}`
  )
    .then(response =>
      response.json().then(json => (response.ok ? json : Promise.reject(json)))
    )
    .then(response => {
      const { uid } = response.user;
      database
        .ref("user_has_roles")
        .child(uid)
        .once("value")
        .then(snapshot => {
          if (snapshot.exists()) {
            dispatch(
              fetchUserFulfilledAction(
                Object.assign({}, response.user, {
                  roleId: snapshot.val().role_id
                })
              )
            );
          } else {
            dispatch(fetchUserFulfilledAction(response.user));
          }
        })
        .catch(e => {
          dispatch(fetchUserRejectedAction(e));
          dispatch(
            error({
              ...notificationOpts(
                {
                  message: e.message
                },
                true
              )
            })
          );
        });
    })
    .catch(e => {
      dispatch(fetchUserRejectedAction(e));
      dispatch(
        error({
          ...notificationOpts(
            {
              message: e.message
            },
            true
          )
        })
      );
    });
};
/**
 * Roles actions
 */

const createUserRequestedAction = () => ({
  type: a.createUserRequested
});

const createUserRejectedAction = e => ({
  type: a.createUserRejected,
  payload: e
});

const createUserFulfilledAction = values => ({
  type: a.createUserFulfilled,
  payload: values
});

export const createUser = (params, callback) => dispatch => {
  dispatch(createUserRequestedAction());
  return fetch(
    "https://us-central1-react-admin-7b185.cloudfunctions.net/CreateUser",
    {
      method: "POST",
      body: JSON.stringify(params)
    }
  )
    .then(response =>
      response.json().then(json => (response.ok ? json : Promise.reject(json)))
    )
    .then(response => {
      callback();
      dispatch(createUserFulfilledAction());
      dispatch(
        success({
          ...notificationOpts({
            message: response.message
          })
        })
      );
    })
    .catch(e => {
      dispatch(createUserRejectedAction(e));
      dispatch(
        error({
          ...notificationOpts(
            {
              message: e.message
            },
            true
          )
        })
      );
    });
};

/**
 * Roles actions
 */

const updateUserRequestedAction = () => ({
  type: a.updateUserRequested
});

const updateUserRejectedAction = e => ({
  type: a.updateUserRejected,
  payload: e
});

const updateUserFulfilledAction = values => ({
  type: a.updateUserFulfilled,
  payload: values
});

export const updateUser = (params, callback) => dispatch => {
  dispatch(updateUserRequestedAction());
  return fetch(
    "https://us-central1-react-admin-7b185.cloudfunctions.net/UpdateUser",
    {
      method: "POST",
      body: JSON.stringify(params)
    }
  )
    .then(response =>
      response.json().then(json => (response.ok ? json : Promise.reject(json)))
    )
    .then(response => {
      callback();
      dispatch(updateUserFulfilledAction());
      dispatch(
        success({
          ...notificationOpts({
            message: response.message
          })
        })
      );
    })
    .catch(e => {
      dispatch(updateUserRejectedAction(e));
      dispatch(
        error({
          ...notificationOpts(
            {
              message: e.message
            },
            true
          )
        })
      );
    });
};

/**
 * User Delete actions
 */

const deleteUserRequestedAction = () => ({
  type: a.deleteUserRequested
});

const deleteUserRejectedAction = e => ({
  type: a.deleteUserRejected,
  payload: e
});

const deleteUserFulfilledAction = values => ({
  type: a.deleteUserFulfilled,
  payload: values
});

export const deleteUser = params => dispatch => {
  dispatch(deleteUserRequestedAction());
  return fetch(
    `https://us-central1-react-admin-7b185.cloudfunctions.net/DeleteUser/${params}`
  )
    .then(response =>
      response.json().then(json => (response.ok ? json : Promise.reject(json)))
    )
    .then(response => {
      dispatch(deleteUserFulfilledAction(params));
      dispatch(
        success({
          ...notificationOpts({
            message: response.message
          })
        })
      );
    })
    .catch(e => {
      dispatch(deleteUserRejectedAction(e));
      dispatch(
        error({
          ...notificationOpts(
            {
              message: e.message
            },
            true
          )
        })
      );
    });
};
