export const actionTypes = {
  /* Fetch Roles */
  fetchRolesRequested: "FETCH_ROLES_REQUESTED",
  fetchRolesRejected: "FETCH_ROLES_REJECTED",
  fetchRolesFulfilled: "FETCH_ROLES_FULFILLED",
  clearProductsRequestedAction: "CLEAR_ROLES",

  /* Save  Roles */
  saveRoleRequested: "SAVE_ROLE_REQUESTED",
  saveRoleRejected: "SAVE_ROLE_REJECTED",
  saveRoleFulfilled: "SAVE_ROLE_FULFILLED",

  /* Delete Roles */
  deleteRoleRequested: "DELETE_ROLE_REQUESTED",
  deleteRoleRejected: "DELETE_ROLE_REJECTED",
  deleteRoleFulfilled: "DELETE_ROLE_FULFILLED",

  /* Fetch Permission */
  fetchPermissionsRequested: "FETCH_PERMISSIONS_REQUESTED",
  fetchPermissionsRejected: "FETCH_PERMISSIONS_REJECTED",
  fetchPermissionsFulfilled: "FETCH_PERMISSIONS_FULFILLED",

  /* Fetch Role Permission */
  fetchRolePermissionRequested: "FETCH_ROLE_PERMISSION_REQUESTED",
  fetchRolePermissionRejected: "FETCH_ROLE_PERMISSION_REJECTED",
  fetchRolePermissionFulfilled: "FETCH_ROLE_PERMISSION_FULFILLED",

  /* Fetch Role Routes */
  fetchRoleRoutesRequested: "FETCH_ROLE_ROUTES_REQUESTED",
  fetchRoleRoutesRejected: "FETCH_ROLE_ROUTES_REJECTED",
  fetchRoleRoutesFulfilled: "FETCH_ROLE_ROUTES_FULFILLED"
};

export default actionTypes;
