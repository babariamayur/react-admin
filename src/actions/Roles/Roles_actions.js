import _ from "lodash";
import { success, error } from "react-notification-system-redux";
import { timestamp, database } from "../../config/firebase_config";
import { actionTypes as a } from "./Actions_types";
import { notificationOpts } from "../../components/Notification/Notification";

/**
 * Roles actions
 */

const fetchRolesRequestedAction = () => ({
  type: a.fetchRolesRequested
});

const fetchRolesRejectedAction = e => ({
  type: a.fetchRolesRejected,
  payload: e
});

const fetchRolesFulfilledAction = values => ({
  type: a.fetchRolesFulfilled,
  payload: values
});

export const fetchRoles = params => dispatch => {
  dispatch(fetchRolesRequestedAction());
  let promise;
  if (params) {
    promise = database.ref("roles").child(params);
  } else {
    promise = database.ref("roles");
  }
  promise
    .once("value")
    .then(snapshot => {
      dispatch(fetchRolesFulfilledAction(snapshot.val()));
    })
    .catch(e => {
      dispatch(fetchRolesRejectedAction(e));
    });
  return promise;
};

/**
 * Role save actions
 */
const roleRequestedAction = () => ({ type: a.saveRoleRequested });
const roleRejectedAction = () => ({
  type: a.saveRoleRejected
});
const roleFulfilledAction = value => ({
  type: a.saveRoleFulfilled,
  payload: value
});

export const exitsFieldValue = (promise, fieldName, fieldValue, key) =>
  new Promise((resolve, reject) => {
    promise
      .orderByChild(fieldName)
      .equalTo(fieldValue)
      .once("value")
      .then(snapshot => {
        const value = snapshot.val();
        if (value) {
          if (_.hasIn(value, key)) {
            resolve(value);
            return true;
          }
          reject({
            message: `<b>${fieldValue}</b> already exits!`
          });
          return true;
        }
        return resolve(value);
      })
      .catch(e => {
        reject(e);
      });
  });

export const saveRole = (params, callback) => {
  const key = params.key || "";
  const permissions = params.permission;
  const routes = params.routes;
  delete params.key;
  delete params.permission;
  delete params.routes;
  return dispatch => {
    dispatch(roleRequestedAction());
    const promise = database.ref("roles");
    Promise.resolve()
      .then(() => exitsFieldValue(promise, "role_name", params.role_name, key))
      .then(() => {
        let roleRefs;
        if (key) {
          roleRefs = promise
            .child(key)
            .update({ ...params, updated_at: timestamp });
        } else {
          roleRefs = promise.push({ ...params, created_at: timestamp });
        }
        roleRefs
          .then(snapshot => {
            const currentKey = key || snapshot.key;
            database
              .ref("role_has_permissions")
              .child(currentKey)
              .set({ ...permissions });
            database
              .ref("role_has_routes")
              .child(currentKey)
              .set({ ...routes });
            promise.once("value").then(values => {
              dispatch(roleFulfilledAction(values.val()));
            });
            callback();
            dispatch(
              success({
                ...notificationOpts({
                  message: `Role has been ${
                    key ? "updated" : "created"
                  } successfully.`
                })
              })
            );
          })
          .catch(e => {
            dispatch(roleRejectedAction(e));
            dispatch(
              error({
                ...notificationOpts(
                  {
                    message: e.message
                  },
                  true
                )
              })
            );
          });
      })
      .catch(e => {
        dispatch(roleRejectedAction(e));
        dispatch(
          error({
            ...notificationOpts(
              {
                message: e.message
              },
              true
            )
          })
        );
      });
    return promise;
  };
};

/**
 * Delete Role Actions
 * */
function deleteRoleRequestedAction() {
  return { type: a.deleteRoleRequested };
}

function deleteRoleRejectedAction() {
  return { type: a.deleteRoleRejected };
}

function deleteRoleFulfilledAction(payload) {
  return { type: a.deleteRoleFulfilled, payload };
}
export const deleteRole = params => {
  const { key } = params;
  return dispatch => {
    dispatch(deleteRoleRequestedAction());
    const promise = database
      .ref("roles")
      .child(key)
      .remove();
    promise
      .then(() => {
        database
          .ref("role_has_permissions")
          .child(key)
          .remove();
        database
          .ref("roles")
          .once("value")
          .then(snapshot => {
            dispatch(deleteRoleFulfilledAction(snapshot.val()));
            dispatch(
              success({
                ...notificationOpts({
                  message: "Role has been deleted successfully."
                })
              })
            );
          });
      })
      .catch(err => {
        dispatch(deleteRoleRejectedAction(key));
        dispatch(
          error({
            ...notificationOpts(
              {
                message: `Problem while removing role item ${
                  params.role_name
                }. ${err.message}`
              },
              true
            )
          })
        );
      });
    return promise;
  };
};

/**
 * Permission fetch actions
 */

const fetchPermissionsRequestedAction = () => ({
  type: a.fetchPermissionsRequested
});

const fetchPermissionsRejectedAction = e => ({
  type: a.fetchPermissionsRejected,
  payload: e
});

const fetchPermissionsFulfilledAction = values => ({
  type: a.fetchPermissionsFulfilled,
  payload: values
});

export const fetchPermissions = params => dispatch => {
  dispatch(fetchPermissionsRequestedAction());
  let promise;
  if (params) {
    promise = database.ref("permissions").child(params);
  } else {
    promise = database.ref("permissions");
  }
  promise
    .once("value")
    .then(snapshot => {
      dispatch(fetchPermissionsFulfilledAction(snapshot.val()));
    })
    .catch(e => {
      dispatch(fetchPermissionsRejectedAction(e));
    });
  return promise;
};

const roleHasPermissionRequestedAction = () => ({
  type: a.fetchRolePermissionRequested
});
const roleHasPermissionRejectedAction = e => ({
  type: a.fetchPermissionsRejected,
  payload: e
});
const roleHasPermissionFulfilledAction = values => ({
  type: a.fetchRolePermissionFulfilled,
  payload: values
});

export const fetchRoleHasPermission = params => dispatch => {
  dispatch(roleHasPermissionRequestedAction());
  const promise = database.ref("role_has_permissions").child(params);
  promise
    .once("value")
    .then(snapshot => {
      dispatch(roleHasPermissionFulfilledAction(snapshot.val()));
    })
    .catch(e => {
      dispatch(roleHasPermissionRejectedAction(e));
    });
  return promise;
};

const roleHasRoutesRequestedAction = () => ({
  type: a.fetchRoleRoutesRequested
});
const roleHasRoutesRejectedAction = e => ({
  type: a.fetchPRoutesRejected,
  payload: e
});
const roleHasRoutesFulfilledAction = values => ({
  type: a.fetchRoleRoutesFulfilled,
  payload: values
});

export const fetchRoleHasRoutes = params => dispatch => {
  dispatch(roleHasRoutesRequestedAction());
  const promise = database.ref("role_has_routes").child(params);
  promise
    .once("value")
    .then(snapshot => {
      dispatch(roleHasRoutesFulfilledAction(snapshot.val()));
    })
    .catch(e => {
      dispatch(roleHasRoutesRejectedAction(e));
    });
  return promise;
};
export default {
  saveRole,
  deleteRole,
  fetchPermissions,
  fetchRoleHasPermission
};
