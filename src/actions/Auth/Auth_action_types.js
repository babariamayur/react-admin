export const actionTypes = {
  loginRequested: "LOGIN_REQUESTED",
  loginRejected: "LOGIN_REJECTED",
  loginFulfilled: "LOGIN_FULFILLED",

  logoutRequested: "LOGOUT_REQUESTED",
  logoutRejected: "LOGOUT_REJECTED",
  logoutFulfilled: "LOGOUT_FULFILLED",

  getOnlineStatusFulfilled: "GET_ONLINE_STATUS_FULFILLED",

  hasPermissionRequested: "HAS_PERMISSION_REQUESTED",
  hasPermissionRejected: "HAS_PERMISSION_REJECTED",
  hasPermissionFulfilled: "HAS_PERMISSION_FULFILLED",

  hasRoleRequested: "HAS_ROLE_REQUESTED",
  hasRoleRejected: "HAS_ROLE_REJECTED",
  hasRoleFulfilled: "HAS_ROLE_FULFILLED",

  hasRoleRoutesRequested: "HAS_ROLE_ROUTES_REQUESTED",
  hasRoleRoutesRejected: "HAS_ROLE_ROUTES_REJECTED",
  hasRoleRoutesFulfilled: "HAS_ROLE_ROUTES_FULFILLED"
};
export default actionTypes;
