import _ from "lodash";
import { error } from "react-notification-system-redux";
import { auth, database } from "../../config/firebase_config";
import ActionTypes from "./Auth_action_types";
import { notificationOpts } from "../../components/Notification/Notification";

/**
 * Login actions
 */
const loginRequestedAction = () => ({ type: ActionTypes.loginRequested });
const loginRejectedAction = e => ({
  type: ActionTypes.loginRejected,
  payload: e
});
const loginFulfilledAction = user => ({
  type: ActionTypes.loginFulfilled,
  payload: user
});

/**
 * Log in user based on provided user/pass
 * @param {*} user
 * @param {*} pass
 */
export const checkIfRoleExists = uid =>
  database
    .ref("user_has_roles")
    .child(uid)
    .once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        return Promise.resolve(snapshot.val());
      }
      auth.signOut();
      return Promise.reject({
        message: "credentials do not match our records"
      });
    })
    .catch(e => {
      auth.signOut();
      return Promise.reject(e);
    });

export const hasUserRole = uid =>
  database
    .ref("roles")
    .child(uid)
    .once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        return Promise.resolve(snapshot.val());
      }
      auth.signOut();
      return Promise.reject({
        message: "credentials do not match our records"
      });
    })
    .catch(e => {
      auth.signOut();
      return Promise.reject(e);
    });

export const hasRolePermission = roleId =>
  database
    .ref("role_has_permissions")
    .child(roleId)
    .once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        return database
          .ref("permissions")
          .once("value")
          .then(permissions => {
            const userPermissons = permissions.val();
            if (!_.isEmpty(userPermissons)) {
              return Promise.resolve(
                Object.keys(userPermissons)
                  .filter(key => snapshot.val().includes(key))
                  .reduce((obj, key) => {
                    obj[key] = userPermissons[key];
                    return obj;
                  }, {})
              );
            }
            return Promise.resolve();
          });
      }
      return Promise.resolve();
    })
    .catch(e => Promise.reject(e));

export const hasRoleRoutes = roleId =>
  database
    .ref("role_has_routes")
    .child(roleId)
    .once("value")
    .then(snapshot => {
      const routes = snapshot.val();
      return Promise.resolve(routes);
    })
    .catch(e => Promise.reject(e));

export const login = (user, pass, callback) => dispatch =>
  new Promise((resolve, reject) => {
    dispatch(loginRequestedAction());
    const promise = auth.signInWithEmailAndPassword(user, pass);
    promise
      .then(snapshot => {
        const uid = snapshot.uid || snapshot.user.uid;
        return checkIfRoleExists(uid).then(role =>
          hasUserRole(role.role_id).then(roles => {
            dispatch({
              type: ActionTypes.hasRoleFulfilled,
              payload: roles
            });
            return Object.assign({}, snapshot, {
              roleId: role.role_id
            });
          })
        );
      })
      .then(snapshot => {
        dispatch(loginFulfilledAction(snapshot));
        resolve(snapshot);
        callback();
        hasRolePermission(snapshot.roleId).then(permissions => {
          dispatch({
            type: ActionTypes.hasPermissionFulfilled,
            payload: permissions
          });
        });
        hasRoleRoutes(snapshot.roleId).then(routes => {
          dispatch({
            type: ActionTypes.hasRoleRoutesFulfilled,
            payload: routes
          });
        });
      })
      .catch(e => {
        dispatch(loginRejectedAction(e));
        dispatch(
          error({
            ...notificationOpts({
              message: e.message,
              position: "tc"
            })
          })
        );
        reject(e);
      });
  });

/**
 * Logout actions
 */

const logoutRequestedAction = () => ({ type: ActionTypes.logoutRequested });
const logoutRejectedAction = () => ({ type: ActionTypes.logoutRejected });
const logoutFulfilledAction = () => ({ type: ActionTypes.logoutFulfilled });

export const logout = () => dispatch => {
  dispatch(logoutRequestedAction());
  const promise = auth.signOut();
  promise
    .then(user => {
      dispatch(logoutFulfilledAction(user));
    })
    .catch(e => {
      dispatch(logoutRejectedAction(e));
    });
  return promise;
};

/**
 * On load of Authentical HOC check for auth status, if already logged in allow access
 */
export const checkAuthStatus = request => dispatch =>
  new Promise((resolve, reject) => {
    if (request) dispatch(loginRequestedAction());
    auth.onAuthStateChanged(
      snapshot => {
        if (snapshot) {
          const { uid } = snapshot;
          checkIfRoleExists(uid).then(roles => {
            if (roles) {
              const mergeRole = Object.assign({}, snapshot, {
                roleId: roles.role_id
              });
              hasUserRole(roles.role_id).then(role => {
                if (role) {
                  dispatch({
                    type: ActionTypes.hasRoleFulfilled,
                    payload: role
                  });
                  hasRolePermission(roles.role_id).then(permissions => {
                    dispatch({
                      type: ActionTypes.hasPermissionFulfilled,
                      payload: permissions
                    });
                  });
                  hasRoleRoutes(roles.role_id).then(routes => {
                    dispatch({
                      type: ActionTypes.hasRoleRoutesFulfilled,
                      payload: routes
                    });
                  });
                  dispatch(loginFulfilledAction(mergeRole));
                  resolve(snapshot);
                } else {
                  dispatch(loginFulfilledAction([]));
                  resolve([]);
                }
              });
            } else {
              dispatch(loginFulfilledAction([]));
              resolve([]);
            }
          });
        } else {
          dispatch(loginFulfilledAction(snapshot));
          resolve(snapshot);
        }
      },
      e => {
        dispatch(loginRejectedAction(e));
        dispatch(
          error({
            ...notificationOpts({
              message: e.message,
              position: "tc"
            })
          })
        );
        reject(e);
      }
    );
  });

export const getOnlineStatus = currentUserId => dispatch => {
  const promise = database.ref(".info/connected");
  promise.on("value", snapshot => {
    const userRef = database.ref(`presence${currentUserId}`);
    userRef.onDisconnect().remove();
    userRef.set(true);
  });
};

export const setLockStatus = userId => {
  console.log(userId);
  const promise = database
    .ref("user_has_roles")
    .child("userId")
    .update({ hasLocked: false });
  return promise;
};
