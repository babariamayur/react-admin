import { connect } from "react-redux";
import * as a from "../../actions/Permissions/Permissions_actions";
import PermissionLists from "../../views/Permissions/PermissionLists";

const mapDispatchToProps = dispatch => ({
  fetchPermissions: () => dispatch(a.fetchPermissions()),
  deletePermission: (params, callback) =>
    dispatch(a.deletePermission(params, callback))
});
const mapStateToProps = state => ({
  notifications: state.notifications,
  permissions: state.permissions
});
const mergeProps = (state, actions, ownProps) => ({
  ...state,
  ...actions,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  PermissionLists
);
