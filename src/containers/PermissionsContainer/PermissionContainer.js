import _ from "lodash";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import * as a from "../../actions/Permissions/Permissions_actions";
import Permissions from "../../views/Permissions/Permissions";

const mapDispatchToProps = dispatch => ({
  fetchPermissions: params => dispatch(a.fetchPermissions(params)),
  savePermission: (value, callback) =>
    dispatch(a.savePermission(value, callback))
});
const mapStateToProps = state => ({
  permissions: state.permissions,
  initialValues: {
    permission_type: _.isEmpty(state.permissions.data)
      ? ""
      : state.permissions.data.permission_type
  }
});
const mergeProps = (state, actions, ownProps) => ({
  ...state,
  ...actions,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  reduxForm({
    form: "PermissionsForm",
    enableReinitialize: true
  })(Permissions)
);
