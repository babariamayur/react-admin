import React, { Component } from "react";
import LockScreenContainer from "./AuthContainer/LockAuthContainer";

class UserActivity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lockScreen: true
    };
    this.goInactive = this.goInactive.bind(this);
  }
  componentDidMount() {
    window.addEventListener("mousemove", this.resetTimer.bind(this), false);
    window.addEventListener("mousedown", this.resetTimer.bind(this), false);
    window.addEventListener("keypress", this.resetTimer.bind(this), false);
    window.addEventListener(
      "DOMMouseScroll",
      this.resetTimer.bind(this),
      false
    );
    window.addEventListener("mousewheel", this.resetTimer.bind(this), false);
    window.addEventListener("touchmove", this.resetTimer.bind(this), false);
    window.addEventListener("MSPointerMove", this.resetTimer.bind(this), false);
    this.startTimer();
    // this.props.setLockStatus();
  }
  componentWillUnmount() {
    window.removeEventListener("mousemove", this.resetTimer.bind(this), false);
    window.removeEventListener("mousedown", this.resetTimer.bind(this), false);
    window.removeEventListener("keypress", this.resetTimer.bind(this), false);
    window.removeEventListener(
      "DOMMouseScroll",
      this.resetTimer.bind(this),
      false
    );
    window.removeEventListener("mousewheel", this.resetTimer.bind(this), false);
    window.removeEventListener("touchmove", this.resetTimer.bind(this), false);
    window.removeEventListener(
      "MSPointerMove",
      this.resetTimer.bind(this),
      false
    );
    window.clearTimeout(this.timeoutId);
  }
  startTimer() {
    this.timeoutId = window.setTimeout(this.goInactive, 500000);
  }
  resetTimer() {
    window.clearTimeout(this.timeoutId);
    this.goActive();
  }
  goInactive() {
    this.setState({ lockScreen: false });
    console.log("enter");
  }
  goActive() {
    this.startTimer();
  }
  render() {
    const { lockScreen } = this.state;
    if (lockScreen) {
      return this.props.children;
    }
    return <LockScreenContainer />;
  }
}
export default UserActivity;
