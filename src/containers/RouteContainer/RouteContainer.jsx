import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import appRoutes from "../../routes/app";
import isAuthenticated from "../AuthHocContainer/AuthHocContainer";
import isNotAutheticated from "./AddPropsToRoute";

const Routes = passingProps => (
  <Switch>
    {appRoutes.map(prop => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={prop.name} />;
      if (prop.collapse) {
        return prop.views.map(child => {
          if (child.secure) {
            return (
              <Route
                key={child.name}
                path={child.path}
                exact={child.exact}
                component={isAuthenticated(child.component, passingProps)}
              />
            );
          }
          return (
            <Route
              key={child.name}
              path={child.path}
              exact={child.exact}
              component={isNotAutheticated(child.component, passingProps)}
            />
          );
        });
      }
      if (prop.secure)
        return (
          <Route
            key={prop.name}
            path={prop.path}
            exact={prop.exact}
            component={isAuthenticated(prop.component, passingProps)}
          />
        );
      return (
        <Route
          key={prop.name}
          path={prop.path}
          exact={prop.exact}
          component={isNotAutheticated(prop.component, passingProps)}
        />
      );
    })}
  </Switch>
);
export default Routes;
