import _ from "lodash";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Notifications from "react-notification-system-redux";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import Sidebar from "../../components/Sidebar/Sidebar";
import Loader from "../../components/Loader/Loader";
import { style } from "../../variables/Variables";
import * as a from "../../actions/Auth/Auth_actions";

export default function(WrappedComponent, passedProps) {
  class Authentication extends Component {
    constructor(props) {
      super(props);
      this.state = {};
    }
    componentWillMount() {
      const { checkAuthStatus, auth } = this.props;
      const { data } = auth;
      const loaderRequest = !!_.isEmpty(data);
      checkAuthStatus(loaderRequest)
        .then(user => {
          if (user) {
            return true;
          }
          return this.handleNotLoggedInState();
        })
        .catch(() => {
          this.handleNotLoggedInState();
        });
    }
    componentDidUpdate(e) {
      if (
        window.innerWidth < 993 &&
        e.history.location.pathname !== e.location.pathname &&
        document.documentElement.className.indexOf("nav-open") !== -1
      ) {
        document.documentElement.classList.toggle("nav-open");
      }
    }
    /**
     * Handle not logged in state
     *
     * @memberof Authentication
     */
    handleNotLoggedInState() {
      this.props.history.push("/login");
    }
    render() {
      const { isLoading } = this.props.auth;
      const newProps = { ...passedProps, ...this.props };
      if (isLoading || typeof isLoading === typeof undefined) {
        return (
          <div className="wrapper">
            <Loader />
          </div>
        );
      }
      return (
        <div className="wrapper">
          <Notifications
            notifications={this.props.notifications}
            style={style}
          />
          <Sidebar {...newProps} />
          <div id="main-panel" className="main-panel">
            <Header {...newProps} />
            <div className="main-content">
              <WrappedComponent {...newProps} />
            </div>
            <Footer />
          </div>
        </div>
      );
    }
  }
  const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(a.logout()),
    getOnlineStatus: currentUserId =>
      dispatch(a.getOnlineStatus(currentUserId)),
    checkAuthStatus: loaderRequest =>
      dispatch(a.checkAuthStatus(loaderRequest)),
    success: message =>
      dispatch(
        Notifications.success({
          title: <span data-notify="icon" className="pe-7s-check" />,
          message,
          position: "tr",
          autoDismiss: 3
        })
      ),
    error: message =>
      dispatch(
        Notifications.error({
          title: (
            <span data-notify="icon" className="material-icons">
              thumb_down
            </span>
          ),
          message,
          position: "tr",
          autoDismiss: 3
        })
      )
  });
  const mapStateToProps = state => ({
    notifications: state.notifications,
    auth: state.auth
  });
  const mergeProps = (state, actions, ownProps) => ({
    ...state,
    ...actions,
    ...ownProps,
    logout: () => {
      ownProps.history.push("/login");
      actions.logout();
    },
    hasUserPermission: permission =>
      _.some(state.auth.hasUserPermissions, ["permission_type", permission]),
    hasUserRoutes: route => {
      if (!_.isEmpty(state.auth.hasUserRoutes)) {
        return _.indexOf(state.auth.hasUserRoutes, route.toLowerCase()) > -1;
      }
      return true;
    }
  });
  Authentication.contextTypes = {
    store: PropTypes.object
  };

  Authentication.propTypes = {
    notifications: PropTypes.arrayOf(PropTypes.any),
    checkAuthStatus: PropTypes.func.isRequired,
    auth: PropTypes.objectOf(PropTypes.any).isRequired,
    history: PropTypes.objectOf(PropTypes.any)
  };

  Authentication.defaultProps = {
    notifications: [],
    history: []
  };
  return connect(mapStateToProps, mapDispatchToProps, mergeProps)(
    Authentication
  );
}
