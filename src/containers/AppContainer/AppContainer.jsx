import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withTranslate } from "react-redux-multilingual";
import RoutesContainer from "../RouteContainer/RouteContainer";

const AppContainer = props => <RoutesContainer {...props} />;

const mapStateToProps = state => ({
  ...state.Intl
});
export default withRouter(
  connect(mapStateToProps)(withTranslate(AppContainer))
);
