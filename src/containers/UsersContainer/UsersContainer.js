import { connect } from "react-redux";
import * as a from "../../actions/Users/User_actions";
import UserLists from "../../views/Users/UserLists";

const mapDispatchToProps = dispatch => ({
  fetchUsers: params => dispatch(a.fetchUsers(params)),
  deleteUser: params => dispatch(a.deleteUser(params))
});
const mapStateToProps = state => ({
  users: state.users
});
const mergeProps = (state, actions, ownProps) => ({
  ...state,
  ...actions,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  UserLists
);
