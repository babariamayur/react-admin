import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import * as a from "../../actions/Users/User_actions";
import Users from "../../views/Users/Users";

const mapDispatchToProps = dispatch => ({
  fetchRoles: params => dispatch(a.fetchRoles(params)),
  getUser: params => dispatch(a.getUser(params)),
  createUser: (params, callback) => dispatch(a.createUser(params, callback)),
  updateUser: (params, callback) => dispatch(a.updateUser(params, callback))
});
const mapStateToProps = state => ({
  users: state.users,
  initialValues: {
    displayName: state.users.user && state.users.user.displayName,
    email: state.users.user && state.users.user.email
  }
});
const mergeProps = (state, actions, ownProps) => ({
  ...state,
  ...actions,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  reduxForm({
    form: "UsersForm",
    enableReinitialize: true
  })(Users)
);
