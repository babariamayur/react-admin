import { connect } from "react-redux";
import * as a from "../../actions/Roles/Roles_actions";
import RoleLists from "../../views/Roles/RoleLists";

const mapDispatchToProps = dispatch => ({
  fetchRoles: () => dispatch(a.fetchRoles()),
  deleteRole: params => dispatch(a.deleteRole(params))
});
const mapStateToProps = state => ({
  notifications: state.notifications,
  roles: state.roles
});
const mergeProps = (state, actions, ownProps) => ({
  ...state,
  ...actions,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  RoleLists
);
