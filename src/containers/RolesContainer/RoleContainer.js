import _ from "lodash";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import * as a from "../../actions/Roles/Roles_actions";
import Role from "../../views/Roles/Role";

const mapDispatchToProps = dispatch => ({
  fetchRoles: params => dispatch(a.fetchRoles(params)),
  fetchPermissions: () => dispatch(a.fetchPermissions()),
  fetchRoleHasPermission: params => dispatch(a.fetchRoleHasPermission(params)),
  fetchRoleHasRoutes: params => dispatch(a.fetchRoleHasRoutes(params)),
  saveRole: (value, callback) => dispatch(a.saveRole(value, callback))
});
const mapStateToProps = state => ({
  roles: state.roles,
  initialValues: {
    role_name: _.isEmpty(state.roles.data) ? "" : state.roles.data.role_name,
    permission:
      (!_.isEmpty(state.roles.hasPermission) && [
        ...state.roles.hasPermission
      ]) ||
      [],
    routes:
      (!_.isEmpty(state.roles.hasRoutes) && [...state.roles.hasRoutes]) || []
  }
});
const mergeProps = (state, actions, ownProps) => ({
  ...state,
  ...actions,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  reduxForm({
    form: "RolesForm",
    enableReinitialize: true
  })(Role)
);
