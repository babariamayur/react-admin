import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import * as a from "../../actions/Auth/Auth_actions";
import Auth from "../../views/Auth/Auth";

const mapDispatchToProps = dispatch => ({
  login: (email, password, callback) =>
    dispatch(a.login(email, password, callback)),
  checkAuthStatus: () => dispatch(a.checkAuthStatus())
});
const mapStateToProps = state => ({
  auth: state.auth,
  notifications: state.notifications
});
const mergeProps = (state, actions, ownProps) => ({
  ...state,
  ...actions,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  reduxForm({
    form: "AuthForm"
  })(Auth)
);
