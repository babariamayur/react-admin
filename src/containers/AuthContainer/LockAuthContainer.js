import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import * as a from "../../actions/Auth/Auth_actions";
import AuthLock from "../../views/Auth/Lock";

const mapDispatchToProps = dispatch => ({
  login: (email, password, callback) =>
    dispatch(a.login(email, password, callback)),
  checkAuthStatus: () => dispatch(a.checkAuthStatus())
});
const mapStateToProps = state => ({});
const mergeProps = (state, actions, ownProps) => ({
  ...state,
  ...actions,
  ...ownProps
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  reduxForm({
    form: "LockForm"
  })(AuthLock)
);
