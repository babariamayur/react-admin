import a from "../../actions/Users/Actions_types";

const INITIAL_STATE = {
  isLoading: false,
  success: false,
  error: false,
  data: [],
  user: {},
  roles: []
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    /** **** Fetch Users ***** */
    case a.fetchUsersRequested:
      return {
        ...state,
        isLoading: true
      };
    case a.fetchUsersFulfilled:
      return {
        ...state,
        isLoading: false,
        data: action.payload,
        user: {}
      };
    case a.fetchUsersRejected:
      return {
        ...state,
        isLoading: false
      };

    /** **** Fetch Users ***** */

    /** ** Fetch Roles */

    case a.fetchRolesRequested:
      return {
        ...state
      };
    case a.fetchRolesFulfilled:
      return {
        roles: action.payload
      };
    case a.fetchRolesRejected:
      return {
        ...state
      };

    /** ** Fetch Roles */
    /** **** Fetch Users ***** */

    case a.fetchUserRequested:
      return {
        ...state,
        isLoading: true
      };
    case a.fetchUserFulfilled:
      return {
        ...state,
        isLoading: false,
        user: action.payload
      };
    case a.fetchUserRejected:
      return {
        ...state,
        isLoading: false
      };

    /** **** Fetch Users ***** */
    // CREATE_USER
    case a.createUserRequested:
      return {
        ...state,
        isLoading: true
      };
    case a.createUserFulfilled:
      return {
        ...state,
        isLoading: false
      };
    case a.createUserRejected:
      return {
        ...state,
        isLoading: false
      };
    // EDIT_USER
    case a.updateUserRequested:
      return {
        ...state,
        isLoading: true
      };
    case a.updateUserFulfilled:
      return {
        ...state,
        isLoading: false
      };
    case a.updateUserRejected:
      return {
        ...state,
        isLoading: false
      };
    // DELETE_USER
    case a.deleteUserRequested:
      return {
        ...state,
        isLoading: true
      };
    case a.deleteUserFulfilled:
      return {
        ...state,
        isLoading: false,
        data: state.data.filter(user => user.uid !== action.payload)
      };
    case a.deleteUserRejected:
      return {
        ...state,
        isLoading: false
      };
    // DELETE_USER
    default:
      return state;
  }
};
