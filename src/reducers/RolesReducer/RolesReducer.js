import a from "../../actions/Roles/Actions_types";

const INITIAL_STATE = {
  isLoading: false,
  success: false,
  error: false,
  data: [],
  permissions: [],
  hasPermission: [],
  hasRoutes: []
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    /** ** Fetch Roles from firebase */
    case a.fetchRolesRequested:
      return {
        ...state,
        isLoading: true
      };
    case a.fetchRolesFulfilled:
      return {
        ...state,
        data: action.payload,
        isLoading: false
      };
    case a.fetchRolesRejected:
      return {
        ...state,
        isLoading: false
      };
    /** ** Fetch Roles from firebase */

    /** ** Fetch Permissions from firebase */

    case a.fetchPermissionsRequested:
      return {
        ...state,
        isLoading: true
      };

    case a.fetchPermissionsFulfilled:
      return {
        ...state,
        permissions: action.payload,
        isLoading: false
      };

    case a.fetchPermissionsRejected:
      return {
        ...state,
        isLoading: false
      };

    /** ** Fetch Roles from firebase */

    /** ** Fetch Has Role Permission from firebase */
    case a.fetchRolePermissionRequested:
      return {
        ...state,
        isLoading: true
      };

    case a.fetchRolePermissionFulfilled:
      return {
        ...state,
        isLoading: true,
        hasPermission: action.payload
      };
    case a.fetchRolePermissionRejected:
      return {
        ...state,
        isLoading: false
      };

    /** ** Fetch Has Role Permission from firebase */

    case a.saveRoleRequested:
      return {
        ...state,
        isLoading: true
      };

    case a.saveRoleFulfilled:
      return {
        ...state,
        isLoading: false,
        data: action.payload
      };

    case a.saveRoleRejected:
      return {
        ...state,
        isLoading: false
      };
    /** ** Fetch Has Role Routes from firebase */

    case a.fetchRoleRoutesRequested:
      return {
        ...state,
        isLoading: true
      };

    case a.fetchRoleRoutesFulfilled:
      return {
        ...state,
        isLoading: true,
        hasRoutes: action.payload
      };
    case a.fetchRoleRoutesRejected:
      return {
        ...state,
        isLoading: false
      };

    // DELETE_ROLES

    case a.deleteRoleRequested:
      return {
        ...state,
        isLoading: true
      };

    case a.deleteRoleFulfilled:
      return {
        ...state,
        data: action.payload,
        isLoading: false
      };

    case a.deleteRoleRejected:
      return {
        ...state,
        isLoading: false
      };

    default:
      return state;
  }
};
