import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import { reducer as notifications } from "react-notification-system-redux";
import { IntlReducer as Intl } from "react-redux-multilingual";
import authReducer from "./authReducer";
import rolesReducer from "./RolesReducer/RolesReducer";
import permissionsReducer from "./PermissionsReducer/PermissionsReducer";
import usersReducer from "./UsersReducer/UsersReducer";

const rootReducers = combineReducers({
  form: formReducer,
  auth: authReducer,
  roles: rolesReducer,
  permissions: permissionsReducer,
  users: usersReducer,
  notifications,
  Intl
});

export default rootReducers;
