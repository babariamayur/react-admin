import a from "../../actions/Permissions/Actions_types";

const INITIAL_STATE = {
  data: [],
  success: false,
  error: false,
  isLoading: false
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    /** ** Fetch Permissions from firebase */
    case a.fetchPermissionsRequested:
      return {
        ...state,
        isLoading: true
      };
    case a.fetchPermissionsFulfilled:
      return {
        ...state,
        data: action.payload,
        isLoading: false
      };
    case a.fetchPermissionsRejected:
      return {
        ...state,
        isLoading: false
      };
    /** ** Fetch Roles from firebase */
    case a.savePermissionRequested:
      return {
        ...state,
        isLoading: true
      };
    case a.savePermissionFulfilled:
      return {
        ...state,
        isLoading: false
      };
    case a.savePermissionRejected:
      return {
        ...state,
        isLoading: false
      };

    // Delete Permission
    case a.deletePermissionRequested:
      return {
        ...state,
        isLoading: true
      };
    case a.deletePermissionFulfilled:
      return {
        ...state,
        data: action.payload,
        isLoading: false
      };
    case a.deletePermissionRejected:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
};
