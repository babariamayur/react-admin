import { actionTypes as a } from "../actions/Auth/Auth_action_types";

const INITIAL_STATE = {
  data: [],
  hasRole: [],
  hasUserPermissions: [],
  hasUserRoutes: []
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case a.loginRequested: {
      return Object.assign({}, state, {
        isLoading: true,
        error: "",
        success: ""
      });
    }
    case a.loginRejected: {
      return Object.assign({}, state, {
        isLoading: false,
        isLoggedIn: false,
        error: action.payload
      });
    }
    case a.loginFulfilled: {
      return {
        ...state,
        isLoading: false,
        success: true,
        isLoggedIn: true,
        data: action.payload
      };
    }
    case a.hasRoleRequested:
      return {
        ...state
      };
    case a.hasRoleFulfilled:
      return {
        ...state,
        hasRole: action.payload
      };
    case a.hasRoleRejected:
      return {
        ...state
      };
    case a.hasPermissionRequested:
      return {
        ...state
      };
    case a.hasPermissionFulfilled:
      return {
        ...state,
        hasUserPermissions: action.payload
      };
    case a.hasPermissionRejected:
      return {
        ...state
      };
    case a.hasRoleRoutesRequested:
      return {
        ...state
      };
    case a.hasRoleRoutesFulfilled:
      return {
        ...state,
        hasUserRoutes: action.payload
      };
    case a.hasRoleRoutesRejected:
      return {
        ...state
      };
    default:
      return state;
  }
};
