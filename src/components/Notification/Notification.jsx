import React from "react";

function rawMarkup(content) {
  return { __html: content };
}
export const notificationOpts = ({ message, position }, fail) => ({
  title: fail ? (
    <span data-notify="icon" className="material-icons">
      thumb_down
    </span>
  ) : (
    <span data-notify="icon" className="material-icons">
      check_circle
    </span>
  ),
  message: <div dangerouslySetInnerHTML={rawMarkup(message)} />,
  position: position || "tr",
  autoDismiss: 3
});

export default notificationOpts;
