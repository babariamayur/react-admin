import React, { Component } from "react";
import PropTypes from "prop-types";
import { Collapse } from "react-bootstrap";
import { NavLink } from "react-router-dom";

class Child extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  activeRoute(routeName) {
    const { location } = this.props;
    return location.pathname.indexOf(routeName) > -1 ? "active" : "";
  }
  parentRoute(routes) {
    const { location } = this.props;
    return routes.indexOf(location.pathname) > -1;
  }
  render() {
    const { icon, name, views, path, translate, hasUserRoutes } = this.props;
    const renderLinks = views.map(prop => {
      if (!prop.hidden) {
        if (hasUserRoutes(prop.name)) {
          return (
            <li key={prop.name} className={this.activeRoute(prop.path)}>
              <NavLink
                to={prop.path}
                className="nav-link"
                activeClassName="active"
              >
                <span className="sidebar-mini">
                  {translate(prop.name)
                    .split(" ")
                    .map(a => a.charAt(0))}
                </span>
                <span className="sidebar-normal">{translate(prop.name)}</span>
              </NavLink>
            </li>
          );
        }
        return false;
      }
      return "";
    });
    return (
      <li className={this.parentRoute(path) ? "active" : false}>
        <a
          href="/#/"
          data-toggle="collapse"
          className="collapsed"
          aria-expanded={this.state.open}
          onClick={e => {
            e.preventDefault();
            this.setState({ open: !this.state.open });
          }}
        >
          <i className={icon} />
          <p>
            {translate(name)}
            <b className="caret" />
          </p>
        </a>
        <Collapse in={this.state.open || this.parentRoute(path)}>
          <div>
            <ul className="nav">{renderLinks}</ul>
          </div>
        </Collapse>
      </li>
    );
  }
}
Child.propTypes = {
  icon: PropTypes.string,
  name: PropTypes.string,
  path: PropTypes.arrayOf(PropTypes.any),
  translate: PropTypes.func.isRequired,
  hasUserRoutes: PropTypes.func.isRequired,
  views: PropTypes.arrayOf(PropTypes.any),
  location: PropTypes.objectOf(PropTypes.any)
};
Child.defaultProps = {
  icon: "",
  name: "",
  path: [],
  views: [],
  location: []
};
export default Child;
