import React, { Component } from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import HeaderLinks from "../Header/HeaderLinks";
import imagine from "../../assets/img/sidebar-2.jpg";
import logo from "../../assets/img/reactlogo.png";
import appRoutes from "../../routes/app";
import Child from "./Child";

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth
    };
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  }

  activeRoute(routeName) {
    const { location } = this.props;
    return location.pathname.indexOf(routeName) > -1 ? "active" : "";
  }

  updateDimensions() {
    this.setState({ width: window.innerWidth });
  }

  render() {
    const { translate, location, hasUserRoutes, auth } = this.props;
    const { data, hasRole } = auth;
    const { displayName } = data || {};
    const sidebarBackground = {
      backgroundImage: `url(${imagine})`
    };
    return (
      <div
        id="sidebar"
        data-active-color="rose"
        data-background-color="black"
        className="sidebar"
        data-color="black"
        data-image={imagine}
      >
        <div className="sidebar-background" style={sidebarBackground} />
        <div className="logo">
          <div className="logo-normal">
            <a className="simple-text" href="/#/">
              {translate("sitename")}
            </a>
          </div>
          <div className="logo-img">
            <img src={logo} alt="logo" />
          </div>
        </div>

        <div className="sidebar-wrapper">
          <div
            className="user"
            role="presentation"
            onClick={e => {
              e.preventDefault();
              this.setState({ open: !this.state.open });
            }}
          >
            <div className="photo">
              <img
                src="http://lbd-pro-react.creative-tim.com/static/media/default-avatar.f653841f.png"
                alt="Avatar"
              />
            </div>
            <div className="user-info">
              <a
                href="/#/"
                data-toggle="collapse"
                className="collapsed"
                aria-expanded={this.state.open}
              >
                <span>
                  {displayName || ""}
                  {/* <b className="caret rotate-180" /> */}
                </span>
                <span className="label label-warning">
                  {hasRole.role_name || ""}
                </span>
              </a>
              {/* <Collapse in={this.state.open}>
                <div>
                  <ul className="nav">
                    <li className="nav-item">
                      <a className="nav-link" href="/#">
                        <span className="sidebar-mini"> MP </span>
                        <span className="sidebar-normal"> My Profile </span>
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="/#">
                        <span className="sidebar-mini"> EP </span>
                        <span className="sidebar-normal"> Edit Profile </span>
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="/#">
                        <span className="sidebar-mini"> S </span>
                        <span className="sidebar-normal"> Settings </span>
                      </a>
                    </li>
                  </ul>
                </div>
          </Collapse> */}
            </div>
          </div>
          <ul className="nav">
            {this.state.width <= 991 ? <HeaderLinks /> : null}
            {appRoutes.map(prop => {
              if (!prop.redirect && !prop.collapse && !prop.hidden) {
                if (hasUserRoutes(prop.name)) {
                  return (
                    <li className={this.activeRoute(prop.path)} key={prop.name}>
                      <NavLink
                        to={prop.path}
                        className="nav-link"
                        activeClassName="active"
                      >
                        <i className={prop.icon}>{prop.icon_title}</i>
                        <p>{translate(prop.name)}</p>
                      </NavLink>
                    </li>
                  );
                }
                return false;
              }
              if (prop.collapse) {
                if (hasUserRoutes(prop.name)) {
                  return (
                    <Child
                      key={prop.name}
                      {...prop}
                      translate={translate}
                      hasUserRoutes={hasUserRoutes}
                      location={location}
                    />
                  );
                }
                return null;
              }
              return null;
            })}
          </ul>
        </div>
      </div>
    );
  }
}
Sidebar.propTypes = {
  translate: PropTypes.func.isRequired,
  hasUserRoutes: PropTypes.func,
  auth: PropTypes.objectOf(PropTypes.any).isRequired
};
Sidebar.defaultProps = {
  hasUserRoutes: () => {}
};
export default Sidebar;
