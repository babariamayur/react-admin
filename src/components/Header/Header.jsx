import React, { Component } from "react";
import PropTypes from "prop-types";
import { Navbar } from "react-bootstrap";
import { IntlActions } from "react-redux-multilingual";
import HeaderLinks from "./HeaderLinks";
import appRoutes from "../../routes/app";

class Header extends Component {
  constructor(props) {
    super(props);
    this.mobileSidebarToggle = this.mobileSidebarToggle.bind(this);
    this.sidebarToggle = this.sidebarToggle.bind(this);
    this.handleLanguage = this.handleLanguage.bind(this);
    this.state = {
      sidebarExists: false
    };
  }
  getBrand() {
    const { location, translate } = this.props;
    let name;
    appRoutes.map(prop => {
      if (prop.collapse) {
        prop.views.map(child => {
          if (child.path === location.pathname) {
            name = translate(child.name);
          }
          return null;
        });
      } else if (prop.redirect) {
        if (prop.path === location.pathname) {
          name = translate(prop.name);
        }
      } else if (prop.path === location.pathname && !prop.hidden) {
        name = translate(prop.name);
      }
      return null;
    });
    return name;
  }
  sidebarToggle(e) {
    if (this.state.sidebarExists === false) {
      this.setState({
        sidebarExists: true
      });
    }
    e.preventDefault();
    document.documentElement.classList.toggle("sidebar-mini");
    const node = document.createElement("div");
    node.id = "bodyClick";
    node.onclick = function() {
      this.parentElement.removeChild(this);
      document.documentElement.classList.toggle("sidebar-mini");
    };
    document.body.appendChild(node);
  }
  mobileSidebarToggle(e) {
    if (this.state.sidebarExists === false) {
      this.setState({
        sidebarExists: true
      });
    }
    e.preventDefault();
    document.documentElement.classList.toggle("nav-open");
    const node = document.createElement("div");
    node.id = "bodyClick";
    node.onclick = function() {
      this.parentElement.removeChild(this);
      document.documentElement.classList.toggle("nav-open");
    };
    document.body.appendChild(node);
  }
  handleLanguage(lang) {
    const { dispatch } = this.props;
    dispatch(IntlActions.setLocale(lang));
  }
  render() {
    return (
      <Navbar fluid className="navbar-transparent navbar-absolute">
        <div
          className="navbar-minimize"
          role="presentation"
          onClick={this.sidebarToggle}
        >
          <button
            className="btn btn-round btn-white btn-fill btn-just-icon"
            id="minimizeSidebar"
          >
            <i className="material-icons visible-on-sidebar-regular">
              more_vert
            </i>
            <i className="material-icons visible-on-sidebar-mini">view_list</i>
            <div className="ripple-container" />
          </button>
        </div>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/#/">{this.getBrand()}</a>
          </Navbar.Brand>
          <Navbar.Toggle onClick={this.mobileSidebarToggle} />
        </Navbar.Header>
        <Navbar.Collapse>
          <HeaderLinks
            handleLanguage={this.handleLanguage}
            logout={this.props.logout}
          />
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
Header.propTypes = {
  location: PropTypes.objectOf(PropTypes.any).isRequired,
  dispatch: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired
};
export default Header;
