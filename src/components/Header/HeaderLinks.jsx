import React from "react";
import PropTypes from "prop-types";
import { NavItem, Nav, NavDropdown, MenuItem } from "react-bootstrap";
import Languages from "../Languages/Languages";

const HeaderLinks = props => {
  const profile = (
    <div>
      <i className="fa fa-list" />
      <p className="hidden-lg hidden-md">Profile</p>
    </div>
  );
  return (
    <div>
      <Nav pullRight>
        <NavItem eventKey={1} href="#">
          <i className="material-icons">dashboard</i>
          <p className="hidden-lg hidden-md">Dashboard</p>
        </NavItem>
        <Languages {...props} />
        <NavDropdown
          eventKey={3}
          title={profile}
          noCaret
          id="basic-nav-dropdown"
        >
          <MenuItem
            eventKey={3.1}
            role="presentation"
            onClick={() => props.logout()}
          >
            Logout
          </MenuItem>
        </NavDropdown>
      </Nav>
    </div>
  );
};
HeaderLinks.propTypes = {
  logout: PropTypes.func.isRequired
};
export default HeaderLinks;
