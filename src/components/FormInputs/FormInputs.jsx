import React from "react";
import PropTypes from "prop-types";
import { Field } from "redux-form";
import classNames from "classnames";
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Row,
  InputGroup
} from "react-bootstrap";

function FieldGroup({ label, className, ...props }) {
  return (
    <FormGroup className={className}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...props} />
    </FormGroup>
  );
}

function FieldInputGroup({ label, className, iconClass, ...props }) {
  return (
    <div className="input-group">
      <InputGroup.Addon>
        <i className="material-icons">{iconClass}</i>
      </InputGroup.Addon>
      <FormGroup className={className}>
        <ControlLabel>{label}</ControlLabel>
        <FormControl {...props} autoComplete={false} />
      </FormGroup>
    </div>
  );
}
const renderField = ({
  input,
  label,
  type,
  iconClass,
  inputGroup,
  meta: { touched, error, warning, active }
}) => {
  const hasValue = !!(touched && input.value);
  const hasError = !!(touched && error);
  const inputClasses = classNames("label-floating", {
    "has-error is-focused": (hasValue && hasError) || (!hasValue && hasError),
    "is-empty": !hasValue && !hasError && !input.value,
    "is-focused": active
  });

  if (inputGroup) {
    return (
      <FieldInputGroup
        label={label}
        type={type}
        className={inputClasses}
        iconClass={iconClass}
        {...input}
      />
    );
  }
  return (
    <FieldGroup label={label} type={type} className={inputClasses} {...input} />
  );
};

const Fields = props => <Field {...props} component={renderField} />;

export const FormInputs = props => {
  const row = [];
  for (let i = 0; i < props.ncols.length; i++) {
    row.push(
      <div key={i} className={props.ncols[i]}>
        <Fields {...props.proprieties[i]} />
      </div>
    );
  }
  return <Row>{row}</Row>;
};
FormInputs.propTypes = {
  proprieties: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired
    }).isRequired
  ).isRequired
};
export default FormInputs;
