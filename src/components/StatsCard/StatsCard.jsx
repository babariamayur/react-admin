import React, { Component } from 'react';

export class StatsCard extends Component{
    render(){
        return (
            <div className="card card-stats">
                <div className="card-header" data-background-color={this.props.statsBgColor}>
                    {this.props.bigIcon}
                </div>
                <div className="card-content">
                    <p className="category">{this.props.statsText}</p>
                    <h3 className="title">{this.props.statsValue}</h3>
                </div>
                <div className="card-footer">
                    <div className="stats">
                       {this.props.statsIcon}{" "}{this.props.statsIconText}
                    </div>
                </div>
            </div>    
        );
    }
}

export default StatsCard;
