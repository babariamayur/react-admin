import React, { Component } from "react";
import classNames from "classnames";
import ContentLoader from "../../components/ContentLoader/ContentLoader";

export class Card extends Component {
  render() {
    return (
      <div
        className={classNames("card", this.props.className, {
          "card-plain": this.props.plain
        })}
      >
        {this.props.cardHeader}
        {this.props.cardCategory ? (
          <p
            className={classNames("category", {
              "text-center": this.props.hCenter
            })}
          >
            {this.props.category}
          </p>
        ) : (
          ""
        )}
        <ContentLoader isLoading={this.props.isLoading}>
          <div
            className={classNames("card-content", {
              "all-icons": this.props.ctAllIcons,
              "table-full-width": this.props.ctTableFullWidth,
              "table-responsive": this.props.ctTableResponsive,
              "table-upgrade": this.props.ctTableUpgrade
            })}
          >
            {this.props.children}
          </div>
          {this.props.cardFooter}
        </ContentLoader>
      </div>
    );
  }
}

export default Card;
