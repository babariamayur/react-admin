import React, { Component } from "react";
import PropTypes from "prop-types";
import BlockUi from "react-block-ui";
import "react-block-ui/style.css";
import Loader from "halogen";

class ContentLoader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { isLoading, children } = this.props;
    return (
      <BlockUi
        tag="div"
        blocking={isLoading}
        loader={<Loader.ClipLoader color="#d81b60" />}
      >
        {children}
      </BlockUi>
    );
  }
}
ContentLoader.propTypes = {
  isLoading: PropTypes.bool,
  children: PropTypes.arrayOf(PropTypes.any).isRequired
};
ContentLoader.defaultProps = {
  isLoading: false
};
export default ContentLoader;
