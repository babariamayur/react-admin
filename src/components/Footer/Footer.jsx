import React, {Component} from 'react';
import { Grid } from 'react-bootstrap';

class Footer extends Component {
	render() {
		return (
            <footer className="footer">
                <Grid>
                    <p className="copyright">
                        &copy; {(new Date()).getFullYear()} <a href="http://www.upforcetech.com/">Upfoce Tech</a>, made with love for a better web
                    </p>
                </Grid>
            </footer>
		);
	}
}

export default Footer;
