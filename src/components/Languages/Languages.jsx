import _ from 'lodash';
import React, {Component} from 'react';
import {languages} from '../../variables/Variables';
import {NavDropdown, MenuItem } from 'react-bootstrap';

export default class DropdownLanguage extends Component {
    constructor(props){
        super(props);
        this.handleDropdown = this.handleDropdown.bind(this);
    }
    handleDropdown(evt){
        //document.body.classList.add("rtl-active");
        const currentLang = _.find(languages, {'key': evt });
        if(_.isObject(currentLang)){
            switch(currentLang.align){
                case 'left':
                    document.body.classList.remove("rtl-active");
                    break;
                case 'right':
                    document.body.classList.add("rtl-active");
                    break;    
                default:
                    document.body.classList.add("rtl-active");
                break;
            }
        }
        this.props.handleLanguage(evt);
    }
    render() { 
        const renderLanguages = (
            languages.map(({key,align}) => <MenuItem key={key} eventKey={key}>{key.toUpperCase()}</MenuItem>)
        )
        const language = (
            <div>
                <i className="material-icons">language</i>
                <p className="hidden-lg hidden-md">Language</p>
            </div>
        );
        return (
            <NavDropdown eventKey={2} title={language} noCaret onSelect={this.handleDropdown} id="basic-nav-dropdown">
                {renderLanguages}
            </NavDropdown>
        );
    }
}