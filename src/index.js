import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from './store/configureStore';
import { AppContainer } from 'react-hot-loader';
import Root from './root/Root';
/**
 * *
 * * Import css **
 * *
 * */
import './assets/css/bootstrap.min.css';
import './assets/css/animate.min.css';
import './assets/sass/light-bootstrap-dashboard.css';
import './assets/css/pe-icon-7-stroke.css';



const store = configureStore();

ReactDOM.render((
        <AppContainer>
            <Root store={ store } />
        </AppContainer>
        ),document.getElementById('root'));

if (module.hot) {
    module.hot.accept('./root/Root', () => {
        const RootContainer = require('./root/Root').default;
        ReactDOM.render(
                <AppContainer>
                    <RootContainer store={ store }/>
                </AppContainer>,
                document.getElementById('root')
                );
    });
}