const functions = require("firebase-functions");
const admin = require("firebase-admin");
const _ = require("lodash");

admin.initializeApp(functions.config().firebase);

const getHasUserRole = snapshot => {
  return admin
    .database()
    .ref("/user_has_roles/")
    .once("value")
    .then(roles => {
      const data = roles.val();
      const users = snapshot.users.map(userRecord => {
        const user = userRecord.toJSON();
        if (typeof data[user.uid] !== typeof undefined) {
          const mergeUserRole = Object.assign({}, user, {
            roleUid: data[user.uid].role_id,
            createdOn: data[user.uid].createdOn
          });
          return mergeUserRole;
        }
        return user;
      });
      return users;
    })
    .catch(e => {
      return e;
    });
};
exports.getAllUser = functions.https.onRequest((request, response) => {
  admin
    .auth()
    .listUsers(1000)
    .then(snapshot => {
      return getHasUserRole(snapshot);
    })
    .then(snapshot => {
      response.writeHead(200, {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      });
      return response.end(
        JSON.stringify({
          success: true,
          users: snapshot
        })
      );
    })
    .catch(error => {
      response.writeHead(401, {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      });
      return response.end(JSON.stringify(error));
    });
});

exports.getUser = functions.https.onRequest((request, response) => {
  const uid = request.url.substring(1);
  admin
    .auth()
    .getUser(uid)
    .then(userRecord => {
      response.writeHead(200, {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      });
      return response.end(
        JSON.stringify({
          success: true,
          user: userRecord.toJSON()
        })
      );
    })
    .catch(error => {
      response.writeHead(401, {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      });
      return response.end(JSON.stringify(error));
    });
});

exports.CreateUser = functions.https.onRequest((request, response) => {
  const requestData = JSON.parse(request.body);
  admin
    .auth()
    .createUser(requestData)
    .then(snapshot => {
      response.writeHead(200, {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      });
      if (!Number.isInteger(requestData.roleId)) {
        admin
          .database()
          .ref("/user_has_roles/" + snapshot.uid)
          .set({
            role_id: requestData.roleId,
            createdOn: admin.database.ServerValue.TIMESTAMP,
            lastLoggedInOn: admin.database.ServerValue.TIMESTAMP
          });
      }
      return response.end(
        JSON.stringify({
          success: true,
          message: "User has been created successfully."
        })
      );
    })
    .catch(error => {
      response.writeHead(401, {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      });
      return response.end(JSON.stringify(error));
    });
});
exports.UpdateUser = functions.https.onRequest((request, response) => {
  const requestData = JSON.parse(request.body);
  const uid = requestData.key;
  admin
    .auth()
    .updateUser(uid, requestData)
    .then(snapshot => {
      response.writeHead(200, {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      });
      if (!Number.isInteger(requestData.roleId)) {
        admin
          .database()
          .ref("/user_has_roles/")
          .child(uid)
          .update({
            role_id: requestData.roleId
          });
      }
      return response.end(
        JSON.stringify({
          success: true,
          user: snapshot.toJSON(),
          message: "User has been updated successfully."
        })
      );
    })
    .catch(error => {
      response.writeHead(401, {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      });
      return response.end(JSON.stringify(error));
    });
});

exports.DeleteUser = functions.https.onRequest((request, response) => {
  const uid = request.url.substring(1);
  admin
    .auth()
    .deleteUser(uid)
    .then(() => {
      response.writeHead(200, {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      });
      admin
        .database()
        .ref("/user_has_roles/")
        .child(uid)
        .remove();
      return response.end(
        JSON.stringify({
          success: true,
          message: "User has been deleted successfully."
        })
      );
    })
    .catch(() => {
      response.writeHead(401, {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      });
      return response.end(JSON.stringify(error));
    });
});
